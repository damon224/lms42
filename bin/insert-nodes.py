import sys
import pickle
import sqlalchemy as sa
import os

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from lms42.app import db

with open("curriculum.pickle", "rb") as file:
    curriculum = pickle.load(file)

with db.engine.connect() as dbc:
    dbc.execute(sa.text("""start transaction"""))
    dbc.execute(sa.text("""delete from nodes"""))

    query = sa.text("""insert into nodes(id,info) values(:id,:info)""")

    for node_id, node in curriculum["nodes_by_id"].items():
        info = {k: v for k, v in node.items() if not k.startswith("assignment") and k != 'resources'}
        dbc.execute(query, id=node_id, info=info)

    dbc.execute(sa.text("""commit"""))
