const PAYLOAD_SIZE = 500;
const MAX_IN_FLIGHT = 16;

const dgram = require('dgram');
const BufferBuilder = require('buffer-builder');
const BufferReader = require('buffer-reader');
const fs = require('fs');

const socket = dgram.createSocket('udp4');
const protoName = Buffer.from("SaxTP");

socket.on('error', (err) => {
	console.error(new Date(), `socket error:\n${err.stack}`);
});

socket.on('message', (buf, rinfo) => {
	// console.log(new Date(), `socket got: ${buf} from ${rinfo.address}:${rinfo.port}`);
	var br = new BufferReader(buf);

	try {
		if (br.buf.length < 5 || !br.nextBuffer(protoName.length).equals(protoName)) {
			sendError(rinfo, "invalid 'SaxTP' signature");
			return;
		}

		if (br.buf.length < 6) {
			sendError(rinfo, "packet type expected");
			return;
		}

		const type = br.nextUInt8();

		if (br.buf.length < 10) {
			sendError(rinfo, "transfer id expected");
			return;
		}

		const transferId = br.nextUInt32BE();
		
		if (type===0) { // Request
			const file = br.nextAll().toString();
			new Request(rinfo, transferId, file);
			return;
		}

		if (type===1) { // ResponseAck
			var req = requests[transferId];
			if (req) {
				if (br.buf.length < 14) {
					sendError(rinfo, "ack id expected");
					return;
				}
				req.ack(br.nextUInt32BE());
			}
			else {
				sendError(rinfo, "ack for non-existent transfer: "+transferId);
			}
			return;
		}
		sendError(rinfo, "invalid packet type received: "+type);
	
	} catch(e) {
		sendError(rinfo, ""+e);
	}
});

socket.bind(29588);



const requests = {};

function Request(rinfo, transferId, file) {
	this.rinfo = rinfo

	if (file=='3.zip' || file=='4.zip') {
		// unreliable mode
		if (Math.random()>0.5) { // drop 50% of initial packets
			console.log(new Date(), rinfo.address, "dropping request", transferId, file);
			return;
		}
		this.unreliable = true;
	}
	console.log(new Date(), rinfo.address, "request", transferId, file);
	if (requests[transferId]) {
		// transfer is still in progress, no need to restart
		this.resetProgressTimeout();
		return;
	}
	this.file = file;
	if (!file.match(/^[a-zA-Z0-9_.]*$/)) file = "";
	fs.readFile("public/"+file, (err,data) => {
		if (err) {
			sendError(rinfo, `no such file '${this.file}'`);
			return;
		}
		this.data = data;
		this.acked = [];
		this.packets = Math.ceil((data.length+1) / PAYLOAD_SIZE);
		requests[transferId] = this;
		this.transferId = transferId;	
		this.send();
		this.resetProgressTimeout();
	});
}

Request.prototype.ack = function(seq) {
	if (this.unreliable && Math.random()<0.1) {
		console.log(new Date(), this.rinfo.address, "dropping ack", this.transferId, seq);
		return;
	}
	console.log(new Date(), this.rinfo.address, "ack", this.transferId, seq);
	this.acked[seq] = true;
	for (var i = 0; i < this.packets; i++) {
		if (this.acked[i] !== true) {
			// transfer not done yet
			this.send();
			this.resetProgressTimeout();
			return;
		}
	}
	console.log(new Date(), this.rinfo.address, "transfer complete", this.transferId);
	this.close();
};

Request.prototype.resetProgressTimeout = function() {
	clearTimeout(this.progressTimeout);
	this.progressTimeout = setTimeout(() => {
		this.close();
		sendError(this.rinfo, "aborting due to ack timeout");
	}, 20000);
};

Request.prototype.close = function() {
	delete this.data;
	delete this.acked;
	delete requests[this.transferId];
	clearTimeout(this.progressTimeout);
};

Request.prototype.send = function() {
	var inFlight = 0;
	for(var seq = 0; inFlight < MAX_IN_FLIGHT && seq < this.packets; seq++) {
		var ack = this.acked[seq];
		if (ack!==true) {
			inFlight++;
			if (ack!==false) {
				this.sendFrame(seq);
			}
		}
	}
};

Request.prototype.sendFrame = function(seq) {
	if (!this.acked || this.acked[seq]===true) return; // already aborted or acked
	this.acked[seq] = false;

	var bb = new BufferBuilder();
	bb.appendBuffer(protoName);
	bb.appendUInt8(128); // packet type ResponseData
	bb.appendUInt32BE(this.transferId);
	bb.appendUInt32BE(seq);
	bb.appendBuffer(this.data.slice(seq * PAYLOAD_SIZE, (seq+1) * PAYLOAD_SIZE));

	if (!this.unreliable || Math.random()>0.1) {
		console.log(new Date(), this.rinfo.address, "sending frame", this.transferId, seq);
		socket.send(bb.get(), this.rinfo.port, this.rinfo.address);
	}
	else {
		console.log(new Date(), this.rinfo.address, "dropping frame", this.transferId, seq);
	}

	setTimeout(() => { // retry in 3s
		this.sendFrame(seq);
	}, 1000);
};

function sendError(rinfo, message) {
	console.error(new Date(), rinfo.address, message);

	var bb = new BufferBuilder();
	bb.appendBuffer(protoName);
	bb.appendUInt8(129); // packet type Error
	bb.appendString(message);

	socket.send(bb.get(), rinfo.port, rinfo.address);
}