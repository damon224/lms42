<Program> ::= <Block> EOF

<Block> ::= <Expression> (EOL+ <Expression>)* EOL*

<Expression> ::= <BinaryOperator> | <Assignment> | <InputOutput> | <IfExpression> | <LoopExpression>

<BinaryOperator> ::= <Atom> ( ("&&" | "||" | "==" | "!=" | "+" | "-" | "*" | "/" | "%") <BinaryOperator> )?

<Atom> ::= "(" <Block> ")" | <Literal> | <VariableReference>

<Assignment> ::= TODO: The 'set' expression.

<InputOutput> ::= TODO: print! str? int? expressions.

<IfExpression> ::= TODO

<LoopExpression> ::= TODO
