# You should not need to change this file.
# (But feel free to do so anyway!)

import re

# The lexing regular expression. It matches everything,
# wrapping every matched token into a named capture group.

matcher = re.compile(r'''
(?P<string>
    \" (?: \\ . | [^"] )* \"
) |
(?P<comment>
    [#].*
) |
(?P<eol>
    \n(\s*\n)*
) |
(?P<whitespace>
    \s+
) |
                     
# TODO: add more token types within this regular expression!
                     
(?P<unknown>
    [\w\W] # Anything else. This should imply an error, the parser should never match this token.
)
''', re.MULTILINE | re.VERBOSE)


class Token:
    def __init__(self, kind, text, line, column):
        self.kind = kind
        self.text = text
        self.line = line
        self.column = column

    def __repr__(self):
        return f'{repr(self.text)} <{self.kind}>'


def tokenize(source) -> list[Token]:
    """Given a saxy source string, returns a list of `Token`s."""
    tokens = []
    line = 1
    column = 1
    for match in matcher.finditer(source):
        text = match.group()
        tokens.append(Token(match.lastgroup, text, line, column))
        
        # Progress the line and column, based on the number of newlines and other
        # characters we have scanned.
        last_newline = text.rfind("\n")
        if last_newline >= 0:
            line += text.count("\n")
            column = len(text) - last_newline
        else:
            column += len(text)

    # Add an end-of-file marker. This makes parsing a bit easier.
    tokens.append(Token('eof', '', line, column))
    return tokens
