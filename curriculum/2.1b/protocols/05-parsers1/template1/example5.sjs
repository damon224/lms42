/*
 * Binary operators and parenthesis
 */

// The * operator
print(21 * 2); 

// The + and - are operators. The results of one operator can be combined with another operator.
// This can either be parsed as ((25 + 25) - 8) or as (25 + (25 - 8)). Both are fine.
print(25 + 25 - 8);

// Parenthesis may be placed around any expression. Our simple language requires them to make 
// sure the multiplication is done before the addition.
print(2 + (2 * 20)); 

// Binary operators can be used for strings as well. (We don't actually make a distinction while
// parsing.)
var brand = input("Favorite car brand? ");
if (brand != "") {
    print("Indeed! " + brand + "s are awesome cars!"); // Multiple + operators
} else {
    print("NO INPUT!");
}

var favorite = parseInt(input("Favorite number? "));
if (favorite > 0) {
    print("The most awesome number in the universe is", favorite, "!");
} else if (favorite < 0) {
    print("Let's be a little more positive with", favorite * -1);
} else {
    print("That's not much!");
}
