name: IP
description: The Internet Protocol.
goals:
    ip: 1
days: 2
depend: linux
assignment:
    The Internet:
    - |
        Lots to learn and lots of videos to watch this lesson! Sit back and enjoy the ride - the internet is rather amazing! 

        *Hint:* have a look at the first objective before you start watching these videos. That should help you to take relevant notes.
    -
        link: https://www.youtube.com/watch?v=h8K49dD52WA
        title: History of the Internet
        info: This is not really technically relevant, but at only 3m40s, it's worth your time!
    Ethernet:
    -
        link: https://www.youtube.com/watch?v=XaGXPObx2Gs&list=PLowKtXNTBypH19whXTVoG3oKSuOcw_XeW&index=1
        title: Ben Eater - Networking tutorial - Sending digital information over a wire
    -
        link: https://www.youtube.com/watch?v=i8CmibhvZ0c&list=PLowKtXNTBypH19whXTVoG3oKSuOcw_XeW&index=4
        title: Ben Eater - Networking tutorial - Analyzing actual Ethernet encoding
        info: This video is optional, but it makes what is explained in the previous video really tangible.
    -
        link: https://www.youtube.com/watch?v=1XrRT0CmzYw&list=PLowKtXNTBypH19whXTVoG3oKSuOcw_XeW&index=6
        title: Ben Eater - Networking tutorial - Frame formats
    
    -
        title: Ethernet
        0: Not so great
        2: Almost great
        4: Great! 
        text: |
            Watch the provided videos and answer the questions in `1_ethernet.md`.

    IP:
    -
        link: https://www.youtube.com/watch?v=rPoalUa4m8E&list=PLowKtXNTBypH19whXTVoG3oKSuOcw_XeW&index=8
        title: Ben Eater - Networking tutorial - The Internet Protocol
    -
        link:  https://www.youtube.com/watch?v=aamG4-tH_m8&list=PLowKtXNTBypH19whXTVoG3oKSuOcw_XeW&index=9
        title: Ben Eater - Networking tutorial - ARP Mapping between IP and Ethernet 
    -
        link: https://www.youtube.com/watch?v=xNbdeyEI-nE&list=PLowKtXNTBypH19whXTVoG3oKSuOcw_XeW&index=10
        title: Ben Eater - Networking tutorial - Looking at ARP and ping packets 
    -
        link: https://www.youtube.com/watch?v=VWJ8GmYnjTs&list=PLowKtXNTBypH19whXTVoG3oKSuOcw_XeW&index=11
        title: Ben Eater - Networking tutorial - Hop-by-hop routing
    -
        link: https://www.youtube.com/watch?v=tAv_eLm7DMk
        title: IT in Three - Ping & Traceroute
    -
        title: IP
        0: Not so great
        2: Almost great
        4: Great!
        text: |
            Watch the provided videos and answer the questions in `2_ip.md`.

    Network address translation:
    -
        link: https://www.youtube.com/watch?v=FTUV0t6JaDA
        title: NAT Explained - Network Address Translation
    -
        link: https://www.youtube.com/watch?v=wg8Hosr20yw
        title: NAT - SNAT, DNAT, PAT & Port Forwarding
    -
        title: Network address translation
        0: Not so great
        2: Almost great
        4: Great!
        text: |
            Watch the provided videos and answer the questions in `3_nat.md`.

    Wireshark:
    -
        link: https://www.youtube.com/watch?v=o6bZ9n40gPM
        title: "Wireshark Tutorial For Beginners"
    - |
        Let's install Wireshark:

        - Install Wireshark using *Add/Remove Software*.
        - Add your Manjaro user account to the group of users that is allowed to inspect all network traffic using `sudo gpasswd -a $USER wireshark` at the command line.
        - Log out of your KDE session and log back in. This will cause the system to reload which groups your user is part of, which now includes the wireshark group.
        - You can now start Wireshark.

        For the next objectives write down your answers in `answers.txt`.

    TCP:
    -
        link: https://www.youtube.com/watch?v=F27PLin3TV0&list=PLowKtXNTBypH19whXTVoG3oKSuOcw_XeW&index=13
        title: Ben Eater - Networking tutorial - TCP connection walkthrough

    -
        title: TCP
        0: Not so great
        2: Almost great
        4: Great!
        text: |
            The `nc` (netcat) command can (among other things) set up a simple TCP connection from the command line. Use the following command to fetch a random quote from the host `djxmmx.net` at TCP port 17.

            You will need to install the *gnu-netcat* Manjaro package for this.

            ```sh
            nc djxmmx.net 17
            ```

            - Capture such a TCP exchange using Wireshark.
            - Apply a Wireshark filter to hide all packets that are not part of the TCP connection.
            - Answer the following questions in the document:
                1. What TCP port was used on your end of the connection?
                2. What is the raw sequence number of the packet containing the actual quote?
                3. What is the raw sequence number of the next (FIN) packet sent by the server?
                4. What's the numerical difference between these two sequence numbers (subtract them), and what does that difference imply?

    -
        title: HTTP
        0: Not so great
        2: Almost great
        4: Great!
        text: |
            ```sh
            echo -e "GET / HTTP/1.0\n\n" | nc google.com 80
            ```

            We just fetched https://google.com - without even using an HTTP client!
            
            Do the above again, capturing the exchange using Wireshark to answer the following questions:

            1. How many TCP packets were used to transmit the HTTP response? What were the minimum and maximum data sizes of these packets?
            2. Google kindly offered us a cookie. What is the name of that cookie?
            3. How many milliseconds did it take from the first SYN up to the moment the full HTTP response was received? (Hint: right click the first packet and select *Set/Unset Time Reference*.)


    DNS:
    -
        link: https://dnsmadeeasy.com/support/what-is-dns/
        title: What is DNS?

    -
        title: DNS
        0: Not so great
        2: Almost great
        4: Great!
        text: |
            Because people don't like remembering and typing IP addresses, DNS was invented such that we can write `sd42.nl` instead of `178.21.117.25`.

            Install the `dig` utility, that is part of Manjaro's `bind` package.

            Use Wireshark to observe how the address for `sd42.nl` is resolved. As your computer caches DNS responses, it can be a bit hard to force a request to happen. The following command forces a request:

            ```sh
            dig sd42.nl
            ```

            1. What's the IP address of the DNS server?
            2. Is TCP or UDP being used?
            3. On what port is the server running?
            4. How long does the server say the response may be cached?


    Network configuration:
    -
        title: Network configuration
        1: 2 out of 5 correct
        2: 3 out of 5 correct
        3: 4 out of 5 correct
        4: All Correct
        text: |
            Please try to find out from Manjaro what network configuration your computer is currently using to connect to the internet, and add it to the document:

            1. IP address
            2. MAC address
            3. Network mask
            4. Default router IP address
            5. DNS server IP address

            **Hint:** The KDE desktop interface can tell you some of these, but you may need to use your Google-fu and command line skills for others.

    DHCP:
    -
        link: https://www.youtube.com/watch?v=S43CFcpOZSI
        title: DHCP Explained

    -
        title: DHCP
        0: Not so great
        2: Almost great
        4: Great!
        text: |
            Of the network configuration items in the previous objective, the MAC address is built into the network card (or sometimes just randomly generated nowadays). However, the other values depend on how you're connected to the internet. They're usually configured automatically using a DHCP server.

            Use Wireshark to see what a DHCP exchange looks like:
            - Start Wireshark capturing.
            - DHCP requests usually only happen when connecting to the network, and then every so many hours. Use the following command to force a new DHCP request immediately: `sudo dhclient -r <WLAN_INTERFACE> && sudo dhclient -1 <WLAN_INTERFACE>`, where `<WLAN_INTERFACE>` is the name Linux assigned to the network interface you're using to connect to the internet.
            - Stop capturing.

            You should see 4 DHCP packets. Describe what happened in your own words.
