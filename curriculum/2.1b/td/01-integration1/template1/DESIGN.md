# Upfront class diagram

```plantuml
class HelloStudent {
    -private_attr: int
    +public_method(arg1: str): int
}

```



# As-built class diagram

Start by copying your above design here, and adapt it to match how you actually implemented it.
