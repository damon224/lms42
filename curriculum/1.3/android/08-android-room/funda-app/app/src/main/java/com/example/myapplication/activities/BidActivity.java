package com.example.myapplication.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.myapplication.AppDatabase;
import com.example.myapplication.HouseDao;
import com.example.myapplication.R;
import com.example.myapplication.models.Bid;
import com.example.myapplication.models.House;

public class BidActivity extends AppCompatActivity {

    TextView houseView;
    EditText nameView;
    EditText amountView;
    Button bidButton;

    House house;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bid);

        houseView = findViewById(R.id.houseView);
        nameView = findViewById(R.id.nameView);
        amountView = findViewById(R.id.amountView);
        bidButton = findViewById(R.id.bidButton);

        AppDatabase db = AppDatabase.getInstance(this);
        HouseDao dao = db.houseDao();

        Intent intent = getIntent();
        if (intent==null) {
            finish();
            return;
        }

        long id = intent.getLongExtra("id", -1);
        if (id < 0) {
            finish();
            return;
        }

        dao.getHouse(id).observe(this, new Observer<House>() {
            @Override
            public void onChanged(House _house) {
                house = _house;
                houseView.setText(house.toString());
            }
        });

        bidButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (house==null) return;

                Bid bid = new Bid();
                bid.name = nameView.getText().toString();
                bid.amount = Integer.parseInt(amountView.getText().toString());
                bid.houseId = house.id;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        dao.addBid(bid);
                    }
                }).start();

                finish();
            }
        });

    }
}