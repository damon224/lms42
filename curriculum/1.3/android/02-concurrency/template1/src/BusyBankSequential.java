import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;

public class BusyBankSequential {
    static final int ACCOUNTS = 1000; // number of bank users in the system
    static final int TRANSACTIONS = 5_000_000; // number of transactions to process
    static final int MAX_TRANSFER = 10_000_000; // the maximum amount in a single transaction

    static private class Transaction {
        int from;
        int to;
        long amount;

        public Transaction(int from, int to, long amount) {
            this.from = from;
            this.to = to;
            this.amount = amount;
        }
    }

    static private class Account {
        long balance;
        byte[] checkSum;
        void addBalance(long amount) {
            balance += amount;
            checkSum = calculateChecksum();
        }
        byte[] calculateChecksum() {
            MessageDigest digest = null;
            try {
                digest = MessageDigest.getInstance("SHA1");
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
            return digest.digest(ByteBuffer.allocate(Long.BYTES).putLong(balance).array());
        }
    }

    Account[] accounts = new Account[ACCOUNTS];


    BusyBankSequential() {
        for(int i = 0; i < accounts.length; i++) {
            accounts[i] = new Account(); // Instantiate each object
        }
    }


    public static void main(String[] args) throws InterruptedException {
        BusyBankSequential bank = new BusyBankSequential();

        // Run the transactions
        long before = System.currentTimeMillis();
        bank.runTransactions();
        long after = System.currentTimeMillis();
        System.out.println("Execution time: " + (after - before) + "ms");

        // Check the resulting balances
        bank.checkBalances();
    }


    /**
     * This method returns the details for one of the transactions. Instead of reading the transaction from a large data
     * file, we're just (deterministically) making a random one up.
     * @param num The number of the transaction (0...TRANSACTIONS) we're interested in.
     * @return The transaction details.
     */
    Transaction getTransaction(int num) {
        if (num<0 || num>=TRANSACTIONS) {
            throw new RuntimeException("Invalid transaction number "+num);
        }
        Random rand = new Random(num);
        return new Transaction(rand.nextInt(ACCOUNTS), rand.nextInt(ACCOUNTS), rand.nextInt(MAX_TRANSFER));
    }


    /**
     * Check to see the current balances reflect all transactions having been properly applied. Throws otherwise.
     */
    void checkBalances() {
        long sum = 0;
        long absSum = 0;
        for(int i=0; i<ACCOUNTS; i++) {
            Account account = accounts[i];
            sum += account.balance;
            absSum += Math.abs(account.balance);
            if (!Arrays.equals(account.calculateChecksum(), account.checkSum)) {
                throw new RuntimeException("Invalid checksum for account "+i+": "+bytesToHex(account.calculateChecksum())+" != "+bytesToHex(account.checkSum));
            }
        }

        if (sum != 0) {
            throw new RuntimeException("Invalid account balance sum "+sum);
        }
        if (absSum != 252541314850L) {
            throw new RuntimeException("Invalid absolute account total "+absSum);
        }
    }

    /** Convert a byte array to a hexadecimal String.
     * @param hash Input byte array.
     * @return Hexadecimal representation of the input hash.
     */
    static String bytesToHex(byte[] hash) {
        return new BigInteger(1, hash).toString(16);
    }

    /**
     * Obtain each of the transactions using getTransaction, and apply them to the balances.
     */
    void runTransactions() {
        for(int num=0; num<TRANSACTIONS; num++) {
            runTransaction(num);
        }
    }

    /**
     * Run a single transaction.
     * @param num The number of the transaction (0...TRANSACTIONS) we want to run.
     */
    private void runTransaction(int num) {
        Transaction tr = getTransaction(num);
        accounts[tr.from].addBalance(-tr.amount);
        accounts[tr.to].addBalance(tr.amount);
    }
}
