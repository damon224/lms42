
# Project management

| Topic | Company name 1 | Company name 2 | Company name 3 |
| -- | -- | -- | -- |
| Issue tracking | Notes on the wall | Internal tool | GitLab |
| Daily standup | ✓ | 𐄂 | ✓ |
| Sprint duration | 2 weeks |No sprints | 3 weeks |
| Developers in the team | 4 | 8 | 6 |
| Issue size estimation | Planning poker | By the scrum master | N/A |
| Code reviews | ....
| Revision control system | ...
| Revision control workflow | ...

*TODO:* The items above are just to get you started. You will need to add more topics and you may want to change the topics that are already there. There should be at least 7, but more can be better. Try to keep texts short (less than 10 words). Your goal is for the viewer to understand the most important differences between the development processes of these companies at a glance.

