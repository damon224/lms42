import priority_queue_tests
import sys


class BinarySearchTree:
    def __init__(self):
        """Create an empty binary search (sub)tree."""
        self.left: None | BinarySearchTree = None
        self.right: None | BinarySearchTree = None
        self.value: None | int = None

    def add(self, value):
        """Add `value` at the correct position to the binary search (sub)tree."""
        # TODO

        # NOTE: Although there is more than one way to approach this, it's easiest to work
        # while always enforcing the following rules:
        # - When `value` is None, `left` and `right` are always None.
        # - When `value` is not None, `left` and `right` always reference BinarySearchTree objects (that may have None `value`s).
        # The check() methods checks these rules (among other things).
    
    def has(self, value):
        """Returns `True` if the (sub)tree contains `value`, and `False` otherwise."""
        # TODO

    def is_empty(self):
        """Returns `True` if the (sub)tree is empty, and `False` otherwise."""
        # TODO

    def to_list(self) -> list:
        """Returns the (sub)tree as an ordered list of values."""
        # TODO

    def fetch_smallest(self):
        """Finds the smallest value in the (sub)tree, removes it from the tree
        and returns it."""
        if self.left and self.left.value is not None:
            value = self.value
            self.remove(value)
            return value

        return self.left.fetch_smallest() if self.left else None

    def remove(self, value):
        """Remove `value` from the (sub)tree. Returns a boolean indicating if
        the value was found."""
        # TODO

    def check(self):
        """Check if the (sub)tree is a consistent binary search tree. Raises
        an exception if a problem is found."""
        result = self._check_recurse()
        if result:
            self.print()
            result.reverse()
            raise Exception(' ➔ '.join(result))

    def _check_recurse(self):
        """Helper function for check()."""
        if self.value is None:
            if self.left is None:
                return ["Empty node should not have children", "left"]
            if self.right is not None:
                return ["Empty node should not have children", "right"]
        else:
            assert self.left and self.right
            if (self.left.value is not None and self.left.value > self.value):
                return ["Left child larger than parent", "left"]
            if (self.right.value is not None and self.right.value < self.value):
                return ["Right child smaller than parent", "right"]
            return self.left._check_recurse() or self.right._check_recurse()
        return None

    def print(self, indent=0):
        """Prints the structure of the (sub)tree."""
        pre = ' '*indent + ' ➔'
        if self.value is None:
            print(pre, 'empty')
        else:
            assert self.left and self.right
            self.left.print(indent + 4)
            print(pre, self.value)
            self.right.print(indent + 4)


if __name__ == '__main__':

    def test(*ops):
        print("\n\nTest: ", end="")
        bst = BinarySearchTree()
        assert bst.is_empty()
        expected = []
        for op in ops:
            print(op, end=" ")
            if op > 0:
                bst.add(op)
                expected.append(op)
            else:
                bst.remove(-op)
                expected.remove(-op)
            bst.check()
            assert bst.is_empty() == (not expected)
        print("")
        bst.print()

        for i in range(20):
            if i in expected and not bst.has(i):
                raise Exception(f"has({i}) returned false")
            if i not in expected and bst.has(i):
                raise Exception(f"has({i}) returned true")

        out = bst.to_list()
        expected.sort()
        if expected != out:
            raise Exception(f"to_list() output ({out}) doesn't match expected expected ({expected})")

    command = sys.argv[1] if len(sys.argv) >= 2 else None

    if command == 'add':
        test(+2, +1, +3)
        test(+5, +2, +6, +1)
        test(+5, +2, +3, +1, +8, +6, +7, +9)

    elif command == 'remove':
        test(+2, +1, +3, -3) # Delete a leaf node
        test(+5, +2, +6, +1, -2) # Delete a node with one child
        test(+5, +2, +3, +1, +8, +6, +7, +9, -5) # Delete a node with two children

    elif command == 'unbalanced':
        test(+1, +2, +3, +4, +5, +6, +7, +8, +9, +10, +11, +12, +13) # Create an unbalanced tree

    elif command == 'priority':
        priority_queue_tests.run_all_tests(BinarySearchTree)

    else:
        print(f"Usage: {sys.argv[0]} {{ add | remove | unbalanced | priority }}")
