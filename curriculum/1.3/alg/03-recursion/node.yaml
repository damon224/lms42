name: Recursion
days: 2
goals:
    recursion: 1
pair: true
resources:
    -
        link: https://www.youtube.com/watch?v=mz6tAJMVmfM
        title: CS50 - Recursion
        info: A good video explanation about why and how to apply recursion.
    -
        link: https://youtu.be/Mv9NEXX1VHc
        title: Computerphile - Recursion
        info: Recursion explained by example with factorial, also introduction of a stack.
    -
        link: https://www.programiz.com/python-programming/recursion
        title: Programiz - Python Recursion
        info: This text is less in-depth, but does use Python examples.
    -
        link: https://www.python-course.eu/python3_recursive_functions.php
        title: Python Course - Recursive Functions
        info: Another Python-based explanation. This also contains exercises and their solutions. We'd recommend you try to do a few of these (without peeking at the solutions too much) before starting on today's assignments.
    -
        link: https://www.youtube.com/watch?v=ngCos392W4w
        title: 5 Simple Steps for Solving Any Recursive Problem
        info: This video is a bit longer, but may be useful if you're struggling to come up with recursive solutions.

assignment:
    Recursive file list:
        text: |
            In the provided `files.py` file, implement the `find_files(path)` function according to its inline documentation.

            If you did that correctly, you should be able to use `files.py` to recursively see all files contained within a directory. For example:

            ```sh
            $ python files.py ~/lms/1.1/static-web/html

            /home/frank/lms/1.1/static-web/html/torvalds.jpg
            /home/frank/lms/1.1/static-web/html/.git/info/exclude
            /home/frank/lms/1.1/static-web/html/.git/refs/heads/master
            /home/frank/lms/1.1/static-web/html/.git/objects/52/18777d32a00edbd21c323ece2e10ed6aad2afb
            ... Removed some more .git files for readability ...
            /home/frank/lms/1.1/static-web/html/.git/COMMIT_EDITMSG
            /home/frank/lms/1.1/static-web/html/privacy.html
            /home/frank/lms/1.1/static-web/html/index.html
            /home/frank/lms/1.1/static-web/html/vanrossum.jpg
            /home/frank/lms/1.1/static-web/html/stallman.jpg
            /home/frank/lms/1.1/static-web/html/carmack.jpg
            /home/frank/lms/1.1/static-web/html/style.css
            ```
        must: true

    Random strings:
        text: |
            In the provided `random_words.py` file, implement the `get_words_recursive` function. It should return all random 'words' for a given set of letters to use and a given number of letters per word. It should do so using recursion. Your implementation should pass the tests in `random_words_tests.py`.
            
            An iterative implementation has already been provided. Notice that it is rather different and more complicated than your recursive implementation.
        must: The tests should pass and recursion should be used (in a way that makes sense).

    Random strings tests:
        text: |
            Add a case to `test_random_words.py` for testing word size `0`. Make sure your implementation still passes.

            Reminder: recursive algorithms should generally just need *one* base case and one recursive case.
        must: true

    Disc puzzle:
        text: |
            Consider the following puzzle game:
            <img src="discs.jpg" class="no-border">
            Given a stack (A) of discs, each one smaller than the ones below. Move all discs to another stack (C) while observing the following rules:

            - Only one disc at a time may be moved. Discs can only be placed on one of three stacks (A, B or C). So stack B can be used as a temporary location.
            - A disc may never be placed on top of a smaller disc.

            A physical instance of this puzzle is available for you to play with, to get an intuitive feel for it. Hint: consider this in terms of recursion, where moving just the smallest disc to another stack is the (trivially easy) base case. Other cases involve moving the smallest X discs to another stack...

            Implement a solver for this puzzle for any number of discs, using recursion. A template (`discs.py`) and a unit test (`discs_test.py`) are provided.

            Below is an example for moving just one disc from A to C. The resulting instructions should be read as: move disc 1 from A to C.
            ```python
            solve('A', 'C', 1) == [('A','C',1)]
            ```
            
            Below is an example for moving two discs from A to C. The resulting instructions should be read as: move disc 1 from A to B, then move disc 2 from A to C, and finally move ring 1 from B to C.
            ```python
            solve('A', 'C', 2) == [('A','B',1), ('A','C',2), ('B','C',1)]
            ```
        must: The tests should pass recursion should be used (in a way that makes sense).

    Code quality:
        ^merge: codequality
        malus: 0

