# These allow you to tweak how many needles there are to be found in how large a haystack.
# Make sure you put these back to 2000000 and 100 and to test with these values before you
# hand in your work.
haystack_size = 2000000
needle_count = 100


def linear_search(haystack: list, needles: list) -> list:
    """Returns a list of indexes, one for each of the needles in the haystack. If the same needle
    has multiple copies in the haystack, the index for just one of the copies will be put in the
    returned list. If a needle does not exist in the haystack, `None` is put in the returned list.

    Arguments:
        haystack {list} -- An ordered list of numbers to be searched in.
        needles {list} -- A list of numbers to search for in haystack.

    Time complexity: O(TODO) where h=len(haystack) and n=len(needles)
    """
    # TODO: implementation!
    return []

def dict_search(haystack: list, needles: list) -> list:
    """Returns a list of indexes, one for each of the needles in the haystack. If the same needle
    has multiple copies in the haystack, the index for just one of the copies will be put in the
    returned list. If a needle does not exist in the haystack, `None` is put in the returned list.

    Arguments:
        haystack {list} -- An ordered list of numbers to be searched in.
        needles {list} -- A list of numbers to search for in haystack.

    Time complexity: O(TODO) where h=len(haystack) and n=len(needles)
    """
    # TODO: implementation!
    return []

def binary_search(haystack: list, needles: list) -> list:
    """Returns a list of indexes, one for each of the needles in the haystack. If the same needle
    has multiple copies in the haystack, the index for just one of the copies will be put in the
    returned list. If a needle does not exist in the haystack, `None` is put in the returned list.

    Arguments:
        haystack {list} -- An ordered list of numbers to be searched in.
        needles {list} -- A list of numbers to search for in haystack.

    Time complexity: O(TODO) where h=len(haystack) and n=len(needles)
    """
    # TODO: implementation!
    return []
