name: Amaze
description: Add a feature to a pre-existing code base.
days: 2
goals:
    legacy_analyze: 1
    legacy_impl: 1
resources:
    -
        link: https://www.youtube.com/watch?v=wN4ZuGruiNw
        title: CUSEC 2016 – How to Read Unfamiliar Code by Josh Matthews - Video
        info: |
            This is a great talk on how not to panic when studying a large unknown code base. Unfortunately, the slides are not visible in the video, but they're available in the next link.
    -
        link: https://www.joshmatthews.net/cusec16/unfamiliar.html
        title: CUSEC 2016 – How to Read Unfamiliar Code by Josh Matthews - Slides
    - |
        Within Visual Studio Code, you can use `ctrl-shift-f` to do a full-text search through the entire project. This can be really helpful, for instance to see where texts you see in the user interface are generated.
    -
        title: Contributing to Open Source for the first time
        link: https://www.youtube.com/watch?v=c6b6B9oN4Vg
        info: A 18m video on how to contribute to an Open Source project using GitHub.
    -
        title: Open Source Guides - How to Contribute to Open Source
        link: https://opensource.guide/how-to-contribute/
        info: This is a great guide, mostly about the social aspects of contributing. Highly recommended!
intro:
    About the exam: |
        <div class="notification">
            The exam for this module consists of a project that requires you to implement something based on your own idea. We recommend that you come up with an idea in advance. The project requirements are available at any time. You may also want to start thinking about what you'd like to contribute to our LMS, which is the next assignment.
        </div>

    Introduction: |
        <wired-card>
            <img src="git-remote.png" class="no-border">
        </wired-card>
        
        In the previous assignment you saw how you can create a Git repository locally (on your own laptop or computer) and commit and revert changes. You have also seen how you can create branches locally and merge or delete them.

        A local Git repository is useful for keeping track of changes you make to your code but no so much if you want to collaborate with others. Using a **remote repository** you can give others - your teammates for example - access to your source code. Remote repositories are located on the internet and depending on the rights, others can view and / or modify the code. The figure on the right shows the relationship between a local repository (on your laptop) and a remote repository. Services like GitLab or GitHub allow you to create remote repositories.
        
        By now you should be familiar with terms like *Working copy*, *Staging area* and your *Local Repository*. A small recap:
        - Using `git add` you move your changes from the working directory to the staging area. 
        - You can save these changes to your local repository with `git commit`. 
        
        With a remote repository, you have to go the extra mile to sync your local repository with the remote repository:
        - With the help of `git push` you can send your latest commits from your local repository to remote. 
        - To update your local repository with the changes from the remote repository, use `git pull` or `git fetch`.

    Tasks:
        - Create a GitLab account: |
            For this assignment you need to have access to a remote repositories on GitLab. Go to [gitlab.com](http://gitlab.com) and sign up for a free account (use your Saxion email address). With this account you can create, clone, push and pull repositories. Be sure to select the role 'Software Developer' in order to get the proper configuration for your account. 
            
            You can skip the parts for creating a new project or organization.

        - Create a SSH key for your GitLab account: |
            There are two ways to access a remote repository, using HTTPS and SSH. The latter is recommended so generate a new SSH key pair and add it your GitLab account. 

            You can generate a key as follows (skip the passphrase):
            ```sh
            ssh-keygen -t ed25519 -C "Captain Awesome's MacBook key"
            ```
            Also see the documentation [here](https://docs.gitlab.com/ee/ssh/index.html#generate-an-ssh-key-pair) on how to generate SSH keys.

            Instruction on how to add your (public) key to GitLab can be found [here](https://docs.gitlab.com/ee/ssh/index.html#add-an-ssh-key-to-your-gitlab-account)

assignment:
    - Create a git repository:
        -
            link: https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-clone
            title: git clone
            info: The 'git clone' command is command line utility which is used to target an existing repository and create a clone, or copy of the target repository.
        -
            text: |
                Your first task is to create a blank repository in GitLab:
                - Open your browser and go to [gitlab.com](http://gitlab.com).
                - Create a new project and select *Create blank project* from the options. 
                - Be sure that your repository is **private** and that you initialize it with a *README*.

                Once you have successfully created your repository then clone it so you can access the code locally on your laptop. You should use the **Clone with SSH** option so that you can push and pull your changes without using a password.

                *Note*: when cloning the repository you have the option to open it in you IDE *Visual Studio Code (SSH)*. For now you should manually clone the repository so you know how this works.
            must: true

    - Pulling in changes:
        -
            link: https://www.atlassian.com/git/tutorials/syncing/git-pull
            title: git pull
            info: The git pull command is used to fetch and download content from a remote repository and immediately update the local repository to match that content.
        -   
            text: |
                In this objective we are going to show how changes can be 
                Do the following:
                - Open your browser and go to your newly created repository [gitlab.com](http://gitlab.com).
                - Your repository should have a *README.md* file. Click on it and click on the **Edit** button (select *Edit single file* option)
                - Edit the *README.md* file in the browser (remove all text and replace it with two lines of your own).
                - Once you are done you click on the **Commit changes** button.

                Now go back to visual studio code and open the *README.md* file on your laptop. You will see that this *README.md* still contains the old text. Use the `git pull` command to synchronize your local repository with remote. What happened to the *README.md*?
            must: true

    - Push it real good:
        -
            link: https://www.atlassian.com/git/tutorials/syncing/git-push
            title: git push
            info: The git push command is used to upload local repository content to a remote repository. Pushing is how you transfer commits from your local repository to a remote repo.
        - 
            text: |
                In a normal work flow you will work in the local repository on your laptop. Before making changes to you code you should pull the latest changes from remote. Make your changes to the codebase, commit these changes and push them to the remote repository. Let's simulate this workflow!

                Tasks:
                - Open the *README.md* file in visual studio code on your laptop. 
                - Add a new line to it (e.g. "This edit was done on my laptop").
                - Commit this change locally (be sure to provide a proper commit message).
                - Then use the `git push` command to send these changes to the remote repository.

                Checks (open your remote repository on [gitlab.com](http://gitlab.com) in a browser):
                - Do you see the updated version of the *README.md*? 
                - Where can you find your commit on [gitlab.com](http://gitlab.com)? 
            must: true
  
    - Making a contribution: 
        - |
            Let's add a feature to an actual Open Source project! Create your own version (also called a *fork*) of the Amaze File Manager using the link below. Clone the source code to your computer and try to get it to compile and run from Android Studio.

        -
            link: https://repo.hboictlab.nl/template/34094632
            title: "Saxion Repo: Amaze File Manager"
            info: Use this page to create your own fork of the *Amaze File Manager* on GitLab.

        - |
            Browse around the source code. It's fairly typical project: code quality is mostly fine but not stellar, documentation is mostly absent, there is some automated testing but it covers only a small part of the functionality (which is especially common for front-ends).

        -
            must: true
            text: Create a new *feature branch* (named `jpg-to-png`) which you'll use to develop the feature described in the next objective.

        -   
            link: https://developer.android.com/reference/android/graphics/BitmapFactory
            title: "Android builtin functions to convert images"
            info: Documentation how the class for BitmapFactory is working, the only thing is implementing it correctly.

        -
            ^merge: feature
            weight: 2
            code: 0
            text: |
                When tapping on the three vertical dots on a `.jpg` file in Amaze, an additional option named *Convert to PNG* should be shown in the context menu that pops up, right underneath the *Encrypt*. Tapping that option should attempt to convert the original file into a PNG file, and write it in the same directory and with the same file name but with the `.png` extension instead of `.jpg`.

                After successful conversion, *PNG created* should be shown as a Toast message. On any error (such a JPG load failure, or permission denied while creating the PNG file), a *Failed to create PNG* Toast should be shown.
            
        -
            1: Two distinct obvious coding style errors.
            2: One obvious coding style error - won't be accepted.
            3: The maintainers might accept this.
            4: Exactly like the solution diff, or better.
            text: |
                Make sure your changes match the project's coding standards.
        
        -
            link: https://docs.gitlab.com/ee/user/project/merge_requests/
            title: Merge requests
            info: Merge requests (MRs) are the way you check source code changes into a branch. When you open a merge request, you can visualize and collaborate on the code changes before merge
        -
            1: 2 of the below.
            2: 3 of the below.
            3: 4 of the below.
            4: Single well-named patch (or multiple well-organized patches), proper copyright, clear and concise description, well-named origin branch, PR to the right branch.
            text: |
                Create a *merge request* for your changes to the `main` branch of your fork. (Normally, you'd create *merge requests* to the *upstream* repository, owned by the maintainers of the official version, but in this case we want you to keep your changes private, so as not to spoil the fun for other students.) You *can* of course have a peek at real-life *pull requests* (the GitHub equivalent of GitLab's *merge requests*) for the [official Amaze repository](https://github.com/TeamAmaze/AmazeFileManager/pulls?q=is%3Apr) for inspiration.

                Make sure that you establish your copyright over your changes in at least the file that you changed most.
        
