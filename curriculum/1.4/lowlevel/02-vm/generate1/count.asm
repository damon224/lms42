counter:
    int16 42
main:
    write8 'x'
    sub16 *counter 1
    if>16 *counter 0 set16 *ip main
    write8 '\n'
    set16 *ip 0
