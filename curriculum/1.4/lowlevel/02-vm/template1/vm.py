#!/usr/bin/env python

import sys

ADDRESS_SIZE = 2 # Memory addresses are expressed as two bytes (16 bits)
IP_ADDRESS = 0 # The *instruction pointer* (the memory address where the next instruction should be read from) is located at address 0.
MEMORY_SIZE = 64*1024 # The computer has 64 kB memory

# The list of instructions, indexable by opcode.
INSTRUCTIONS = ["INVALID", "set", "add", "sub", "mul", "div", "and", "or", "xor", "if=", "if>", "if<", "read", "write"]

# Parse the command line arguments, to get the filename and to find out if we
# should run in debug mode (which should output a lot of info about what the
# virtual CPU is doing).
debug = False
arg_pos = 1
if len(sys.argv)>=2 and sys.argv[1]=="-d":
    debug = True
    arg_pos += 1
if len(sys.argv) != arg_pos+1:
    sys.exit(f"Syntax: {sys.argv[0]} [-d] <program_file>")
filename = sys.argv[arg_pos]

# Create a bytearray that will serve as the virtual computer's memory, and load
# the program into it.
memory = bytearray(MEMORY_SIZE)
with open(filename, 'rb') as file:
    program = file.read()
memory[0:len(program)] = program


# What follows are a few utility functions that you may want to use in your implementation.

def load(addr, size):
    """Returns the integer starting at address `addr` consisting of `size` bytes."""
    return int.from_bytes(memory[addr:addr+size], byteorder='little', signed=False)

def store(addr, value, size):
    """Stores the integer `value` using `size` bytes starting at address `addr`."""
    memory[addr:addr+size] = value.to_bytes(length=size, byteorder='little')

def read_console_byte():
    """Wait for a single byte to be typed by the user and returns it as a number. To be used by instruction 12 (read)."""
    ch = sys.stdin.read(1)
    return 256 if ch=='' else ord(ch)

def write_console_byte(value):
    """Write a single byte (given as the number `value`) to the console. To be used by instruction 13 (write)."""
    sys.stdout.write(chr(value&255))
    sys.stdout.flush()



# TODO: the actual CPU simulation loop!
