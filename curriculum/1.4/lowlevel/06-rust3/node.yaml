name: Generics, traits & lifetimes
goals:
    rust-generics: 1
    pointers: 1
pair: true
resources:
    -
        link: https://www.koderhq.com/tutorial/rust/generics/
        title: Rust Generics Tutorial
        info: A relatively gentle introduction to Rust generics.
    -
        link: https://doc.rust-lang.org/book/ch10-00-generics.html
        title: Generic Types, Traits, and Lifetimes
        info: This chapter in *the book* covers a lot more ground than the tutorial above. This stuff is not easy, so take some time for it, and feel encouraged to search for other tutorials if there's a part that doesn't click for you yet.
    -  
        link: https://www.youtube.com/watch?v=nvur2Ast8hE
        title: Rust Generics
        info: A video on generics and traits.

assignment:
    - Assignment: |
        Your goal for today is to implement the *hash table* data structure using *open addressing* (Google is your friend!) as a *generic* struct in Rust. We'll split the work into multiple parts that should provide you with some guidance. For each step, we have provided a unit test. Before moving on to the next step, it would be wise to do a `git commit`, to easily spot the differences in case things stop working.

    - "`Hashable` traits":
        ^merge: feature
        text: |
            Create a new trait called `Hashable` that has a single method `get_hash`. Implement this trait for `u32` (the implementation itself in this case should be straightforward).

            Uncomment the unit test `u32_is_hashable` and make sure it runs (`cargo test`).

    - Basic u32 hash:
        ^merge: feature
        text: |
            Create a `struct Hash` that works only for `u32` values. So no need to use generics yet. Also, the size of the vector your hash uses can be a constant for now: 59. (This means that the hash can not contain more than 59 elements, and that it gets slow when the number of elements gets close to 59.)

            Each item in the vector can either by a `u32` value or empty. (What Rust built-in enum helps you do this?)

            Implement the `new`, `add` (adds a number to the hash) and `contains` (returns a boolean indicating if a number is contained in the hash) methods for `Hash`.

            Verify that the `hash_works_with_u32` test compiles and runs.

    - Scaling u32 hash:
        ^merge: feature
        text: |
            Modify your implementation such that the vector now automatically scales with the number of elements. When the number of elements in the hash (maintain a counter for this) grows to over 2/3 of the size of the vector, replace the vector with a new vector that is sized for 3 times the current number of elements. Items from the old vector should be moved into the new vector (hint: the `drain(..)` method might help), recalculating the index for each item. This is called 'reindexing'.

            Verify that the `hash_scales_with_u32` test compiles and runs quickly (within a second). Make sure to use `--release` mode, as it can sometimes be tens of times faster.

    - "`Hashable &str`":
        ^merge: feature
        text: |
            Implement `Hashable` for `&str`. Your implementation should work on strings of any length, and should (usually) give different values for strings that are very similar.

            Verify that the `str_ref_is_hashable` test compiles and runs.

            *Hint:* Each byte in a string is a 8-bit (binary) number. These can be combined into a single 32-bit number with some bitshifting (or even better: [bit rotation](https://doc.rust-lang.org/std/primitive.u32.html#method.rotate_left)) and `xor`ing.

    - Generic:
        ^merge: feature
        weight: 2
        text: |
            Modify your `Hash` implementation to by able to work with any data type that implements the `Hashable` trait and the `Eq` trait (a built-in trait for checking equality using the `==` operator).

            Verify that the `hash_works_with_str_ref` test compiles and runs.

    - Lifetimes:
        1: Fixed but no explanation.
        2: "'Explained' using just the compiler error."
        4: Perfect explanation in own words.
        text: |
            Uncomment the last test (`hash_works_with_lifetimes`). It should give a compile-time error. Fix the error (by moving a single line), and explain *in your own words* in the comments why the problem occurred and how your change fixed it.
