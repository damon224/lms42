class BitReader:
    def __init__(self, _bytes):
        self.bytes = _bytes # an array of bytes
        self.byte_offset = 0 # the next byte to read from
        self.bit_offset = 7 # the next bit in that byte to read from

    def get_bit(self):
        """Returns 0 or 1 and moves on to the next bit.
        For each byte, the most significant bit is returned first."""

        # TODO!        

    def get_number(self, bit_count):
        """Read `bit_count` bits and return them as an unsigned number.
        As with get_bit(), the most significant bit from a byte is read first. And the
        first bit to be read will be the most significant bit in the resulting number.
        """

        return "TODO"
        # Hints:
        # - use get_bit()
        # - study the unit test and use it to make sure you get this right


    def get_signed_number(self, bit_count):
        """Like `get_number`, only the bits are interpreted as a Two's Complement
        signed binary number."""

        return "TODO"
        # Hint:
        # - one way is to do this is to use get_number() and modify the result if it should be negative
        # - you can also start out with a copy of your get_number() implementation, and remember that
        #   the most significant bit represents a negative number (so -128 instead of 128, when bit_count
        #   is 8).
    