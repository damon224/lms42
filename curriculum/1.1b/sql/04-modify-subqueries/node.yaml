name: Modifying data and subqueries
goals:
    subqueries: 1
    queries: 1
pair: true
assignment:

    Introduction: |
        This lesson consists of two parts:
        
        - Modifying data in the database using `INSERT`, `UPDATE` and `DELETE` queries.
        - The use of *subqueries*, as an extra tool to get the desired information from your data, and to allow you to modify your data in bulk.

        We'll, for the last time, be working on the DVD rental database.


    Important notes: |

        - For each of the questions below, we expect your answer to be a *single* query, unless stated otherwise.
        - Your queries should not contain literal ids or other information that was not given in the question. So when the assignment says to show all info for *Frank*, you can't do something like `SELECT * FROM teachers WHERE id=42` but you need to do something like `SELECT * FROM teachers WHERE name='Frank'`.


    How to work on the assignment: |
        In earlier assignments, a database including all data was provided by your teachers. Data never changed. This assignment is all about changing data though. Here are a few important things to know:

        - Changes made to the database by running an insert/update/delete query using `ctrl-shift-b` are *not* actually persisted to the database. The next time you `ctrl-shift-b`, the data starts out exactly as it was the first time. This is not how databases usually work, of course, but it *is* very convenient to be able to play around without breaking stuff.
        - When you run an SQL block using `ctrl-shift-b`, all of the SQL blocks *above* that block will be executed first.

    Inserting rows:
    - Basic inserts: |
        Let's say we want to insert a new *category* called *Test* into the database. We can do that like this:

        ```sql
        INSERT INTO category(id, name)
        VALUES(999, 'Test1');
        ```

        The `(id, name)` part list the names of the column we want to provide data for, while the `(999, 'Test')` contains the actual data in the same order.

    - Default values and auto-incrementing ids: |
        So how did we come up with *999*? We actually just guessed that this was a free id - if it turned out to be taken, our `INSERT` query would fail. Bad idea. There's a better way:
        
        ```sql
        INSERT INTO category(name)
        VALUES('Test2');
        ```

        Here, we leave the `id` column unspecified. That is allowed, because the `id` column has a default value defined. (We'll learn about defining columns with default values later.) The default value for `id`, as is common for `id`s, is an auto-incrementing number. So in effect, each insert for which `id` is not specified, will assign a new unique `id` value.

        If we want to know which `id` value PostgreSQL just generated for the last `INSERT` query in our session, we can use the following query:

        ```sql
        SELECT LASTVAL();
        ```

    - Multiple rows at once: |
        We can insert multiple rows in a single query like this:

        ```sql
        INSERT INTO category(name)
        VALUES
            ('Test3'),
            ('Test4');
        ```

    -
        link: https://www.w3schools.com/sql/sql_insert_into_select.asp
        title: SQL INSERT INTO SELECT Statement
        info: Apart from directly specifying the data for the new row(s) within your query, as we've shown above, it is also possible to use the results from a `SELECT` to create new rows to be `INSERT`ed. This tutorial explains how.

    -
        link: https://www.youtube.com/watch?v=nEndOUQFaOI
        title: SQL Into - How to Copy Table Data with Select Into Statement
        info: A video explaining the same as the above tutorial.

    -
        ^merge: queries
        weight: 1.5


    Updating rows:
    -
        link: https://www.youtube.com/watch?v=B6WCH3X9oyE
        title: How to use the SQL UPDATE Statement
        info: Learn how to use the SQL UPDATE Statement. This tutorial demonstrates how to write an UPDATE query with an easy to follow example.
    -
        link: https://www.zentut.com/sql-tutorial/sql-update/
        title: SQL UPDATE Statement
        info: In this tutorial, you will learn how to use SQL UPDATE statement to modify existing data in a table.

    -
        ^merge: queries


    Deleting rows:
    -
        link: https://www.youtube.com/watch?v=VKlNt3pkLaw
        title: How to use the SQL DELETE Statement
        info: Learn how to use the SQL DELETE Statement. This tutorial demonstrates how to write a DELETE query with an easy to follow example.
    -
        link: https://www.zentut.com/sql-tutorial/sql-delete/
        title: SQL DELETE
        info: In this tutorial, you will learn how to use SQL DELETE statement to remove one or more rows in a table.

    -
        ^merge: queries


    Subqueries:
    -
        link: https://www.dofactory.com/sql/subquery
        title: SQL Subqueries
        info: A subquery is a SQL query within a query. They are nested queries that provide data to the enclosing query.
    -
        link: https://www.youtube.com/watch?v=GpC0XyiJPEo
        title: How to do Subqueries in SQL with Examples
        info: Step-by-step tutorial shows you how to use SQL subqueries in your SELECT statement & FROM and WHERE clauses!

    - SQL coding conventions: |
        Please use the following formatting for subqueries:

        ```sql
        SELECT t.first_name, t.last_name,
        FROM teachers AS t
        WHERE t.id IN (
            SELECT p.teacher_id
            FROM projects AS p
            GROUP BY p.teacher_id
            HAVING COUNT(*) > 3
        );

        SELECT *
        FROM projects AS p
        WHERE p.teacher_id = (
            SELECT t.id
            FROM teachers AS t
            WHERE t.first_name = (
                SELECT pn.name
                FROM popular_names AS pn
                ORDER BY pn.popularity DESC
                LIMIT 1
            )
        );

        SELECT AVG(number), AVG(budget)
        FROM (
            SELECT p.category, COUNT(*) AS number, SUM(p.budget) AS budget
            FROM projects AS p
            GROUP BY p.category
        ) AS categories;

        SELECT p.teacher_id, (
            SELECT t.first_name
            FROM teacher AS t
            WHERE t.id = p.teacher_id
        ) AS first_name
        FROM projects AS p
        GROUP BY p.teacher_id
        HAVING COUNT(*) > 3;
        ```

        The above queries can also be used as a quick cheat sheet for the various forms a subquery can take.

        So we're adding the following formatting rules:
        * You should start subqueries on a new line.
        * Subqueries should be indented.
        * The opening bracket `(` should be located on the line before the new line and the closing bracket `)` should be placed on a new line, aligning with the start of the line with the matching opening bracket.
    -
        ^merge: queries
        weight: 2
      

