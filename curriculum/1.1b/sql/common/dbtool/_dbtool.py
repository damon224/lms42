#!/bin/env python

import sys
import os
import subprocess
import random
import json
import time
import postgresqlite
import re
import socket
import traceback
from collections import defaultdict


DBTOOL_DIR = os.path.dirname(__file__)
SOCKET_FILE = ".dbtool-ts.socket"
PID_FILE = ".dbtool-ts.pid"
LOG_FILE = ".dbtool-ts.log"
LOG_QUERIES = 'logical' # 'real' / 'logical' / None


COLOR_RED = "\u001b[31m"
COLOR_MAGENTA = "\u001b[35m"
COLOR_CYAN = "\u001b[36m"
COLOR_RESET = "\u001b[0m"
COLOR_BRIGHT = "\u001b[1m"
COLOR_DIM = "\u001b[2m"


# Connect to our PostgreSQL database
init_db_cache = None
def init_db():
    global init_db_cache
    if not init_db_cache:
        config = postgresqlite.get_config()
        db = postgresqlite.connect(config=config)
        db._paramstyle = 'qmark'
        
        if LOG_QUERIES == 'real':
            db_execute_org = db.execute
            def db_execute_log(*args, **kwargs):
                print(*args, end="\n\n", flush=True)
                return db_execute_org(*args, **kwargs)
            db.execute = db_execute_log

        db.execute('SET statement_timeout = 60000') # abort queries after 60s
        
        env = dict(os.environ)
        env.update(config.env)
        type_map = {row['oid']: row['typname'].rstrip("0123456789") for row in db.execute('select oid,typname from pg_type')}
        init_db_cache = type('db_obj', (object,), {"db": db, "type_map": type_map, "env": env})
    return init_db_cache

def close_db():
    global init_db_cache
    init_db_cache = None


def time_to_seconds(s):
    if s.endswith('ms'):
        return float(s[:-2]) / 1000
    if s.endswith('s'):
        return float(s[:-1])
    raise ValueError("Time requires a 's' or 'ms' suffix")

def table_to_list(s):
    lines = s.split("\n")
    if lines[1].replace('-', '').replace('|', '').strip():
        raise ValueError(f"Invalid table header:\n{s}")
    results = []
    for line in lines[0:1] + lines[2:]:
        results.append([field.strip() if field.strip() != "*NULL*" else None for field in line.strip("| \t").split(' | ')])
    for index in range(len(results)-1):
        if len(results[index]) != len(results[index+1]):
            raise ValueError(f"Table has inconsistent column count at line {index+2}:\n{s}")
    return results

def list_to_table(rows):
    result = "\n"
    for index, row in enumerate(rows):
        result += "| " + " | ".join([str(field) if field is not None else "*NULL*" for field in row]) + " |\n"
        if index == 0:
            result += "| " + " | ".join(["---" for _ in row]) + " |\n"
    return result

def str_to_list(text):
    return text.split(' ')

def list_to_str(rows):
    return ' '.join(rows)

def seconds_to_time_limit(s):
    unit = 's'
    if s < 0.5:
        s *= 1000
        unit = 'ms'
    s = to_upper_bound(s)
    return str(s) + unit


def check_equal(value, expected):
    if type(value) is type(expected) is list and len(value) == len(expected):
        for i in range(len(value)):
            if value[i] != expected[i] and expected[i] != '???':
                return f"should be {expected}, got {value}"
        
    elif value != expected and expected != '???':
        return f"should be {expected}, got {value}"
    
    return None

def check_equal_onordered(value,expected):
    return check_equal(sorted(value), sorted(expected))

def check_limit(value, limit):
    if value > limit:
        return f"should be <= {limit}, got {value}"

def row_to_strings(row):
    return [None if field is None else (re.sub('[.]0$', '', str(field)) if type(field) is float else str(field)) for field in row]

def check_results(value, expected):
    if len(value) != len(expected):
        return f"should have {len(expected)-1} rows, got {len(value)-1}"
    for index in range(len(value)):
        err = check_equal(row_to_strings(value[index]), expected[index])
        if err: return f"{err} at row {index}"

def check_unordered_results(value, expected):
    if len(value) != len(expected):
        return f"should have {len(expected)-1} rows, got {len(value)-1}"
    if value[0] != expected[0]:
        return f"should have columns {expected[0]}, got columns {value[0]}"

    expected_set = {str(row) for row in expected[1:]}

    for row in value[1:]:
        row_str = str(row_to_strings(row))
        if row_str not in expected_set:
            return f"unexpected row {row_str}"
    return None

def check_first_results(value, expected):
    return check_results(value[0:len(expected)], expected)

def to_upper_bound(num):
    num = round(num * 10)
    zeros = 0
    while num > 9:
        zeros += 1
        num //= 10
    num += 1
    return num * (10 ** zeros)


EXPECT_TYPES = dict(
    column_names = (str_to_list, list_to_str, check_equal,),
    column_types = (str_to_list, list_to_str, check_equal,),
    row_count = (int, str, check_equal,),
    modified_row_count = (int, str, check_equal,),
    column_count = (int, str, check_equal,),
    run_time = (time_to_seconds, seconds_to_time_limit, check_limit,),
    table_count = (int, str, check_equal,),
    tables = (str_to_list, list_to_str, check_equal_onordered,),
    results = (table_to_list, list_to_table, check_results,),
    unordered_results = (table_to_list, list_to_table, check_unordered_results,),
    first_results = (table_to_list, list_to_table, check_first_results,),
    query_count = (int, str, check_limit,),
    sql = (str, str, None,),
)


def parse_md(filename, up_to_line=None):
    with open(filename) as file:
        lines = file.readlines()
    result = defaultdict(dict)
    query_key = ""
    mode = None
    buffer = ""
    for line_number, line in enumerate(lines + [""]):
        if mode:
            if (line.startswith("```") if mode=="sql" else line.strip() == ""):
                if mode in result[query_key]:
                    if mode != 'sql':
                        raise ValueError(f"Duplicate '{mode}' in {query_key}")
                    # Concat sql within a section together
                    result[query_key][mode] = result[query_key][mode].rstrip(";") + ";\n" + buffer.rstrip("\n")
                else:
                    result[query_key][mode] = EXPECT_TYPES[mode][0](buffer.rstrip("\n"))
                mode = None
                buffer = ""
            else:
                buffer += line
        elif line.startswith("### "):
            if up_to_line != None and line_number > up_to_line:
                break
            line = line.strip("# \t\n\r")
            query_key = line.split(':',1)[0]
        elif line.startswith("```sql"):
            if up_to_line != None and line_number > up_to_line:
                break
            mode = "sql"
        elif line.startswith("Expected ") and ":" in line:
            name, value = line[9:].split(':', 2)
            name = name.lower().replace(' ', '_')
            if name not in EXPECT_TYPES:
                print(f"Unknown '{line.strip()}' in section {query_key}")
                sys.exit(1)
            value = value.strip()
            if value == "": # a multi-line value, concluded by an empty line
                mode = name
            else:
                result[query_key][name] = EXPECT_TYPES[name][0](value)

    return result



def search_plan(plan, target_key):
    result = []
    if isinstance(plan, list):
        for item in plan:
            result += search_plan(item, target_key)
    elif isinstance(plan, dict):
        for v in plan.values():
            result += search_plan(v, target_key)
        if target_key in plan:
            result.append(plan[target_key])
    return result


def split_queries(sql):  # noqa: C901
    queries = [""]
    quote = None
    client_command = False
    prev_ch = None
    for ch in sql:
        if quote != "\n": # skip comments
            queries[-1] += ch
        if quote: # ignore chars up to quote
            if ch == quote:
                quote = None
        elif client_command:
            if ch == "\n":
                queries.append('')
                client_command = None
        elif ch == ';': # end of query
            queries.append('')
        elif ch in {'"', "'"}: # start a string/identifier
            quote = ch
        elif ch == '-' and prev_ch == '-': # comment until end of line
            queries[-1] = queries[-1][:-2]
            quote = "\n"
        elif ch == '\\' and prev_ch in {None, '\n'}: # client-command like '\d table'
            client_command = True
        prev_ch = ch
    return [query.strip() for query in queries if query.strip()]



class TransactionStack:
    def __init__(self):
        self.db = None

    def start(self):
        """Returns `True` if a new database connection was set up, or `False` when the
        connection was recycled (allowing reuse of the transaction cache)."""
        self.index = 0
        if self.db:
            try: # Test if the db connection is still valid
                self.db.execute("SELECT 1")
                return False
            except postgresqlite.pg8000.exceptions.InterfaceError:
                close_db()
        
        self.db = init_db().db
        self.index = 0
        self.saved_queries = []
        return True

    def _rollback_to(self, index):
        while index < len(self.saved_queries) and self.saved_queries[index] == None:
            index += 1
            if index >= len(self.saved_queries):
                return
        if index > 0:
            self.db.execute(f"ROLLBACK TO SAVEPOINT stack{index}")
        elif self.saved_queries:
            self.db.execute("ROLLBACK")

    def run(self, query, allow_cache=True, log=True, args=()):
            
        if self.index < len(self.saved_queries):
            if allow_cache and self.saved_queries[self.index] == (query,args):
                self.index += 1
                if LOG_QUERIES == 'logical' and log:
                    print(COLOR_BRIGHT+query+COLOR_RESET, end=f" {COLOR_MAGENTA}[cached]{COLOR_RESET}\n\n", flush=True) # magenta [cached]
                return
            self._rollback_to(self.index)

            self.saved_queries = self.saved_queries[:self.index]

        if LOG_QUERIES == 'logical' and log:
            print(COLOR_BRIGHT+query+COLOR_RESET, end="\n\n", flush=True)

        if self.index > 0:
            self.db.execute(f"SAVEPOINT stack{self.index}")
        else:
            self.db.execute("START TRANSACTION")

        self.index += 1
        self.saved_queries.append((query,args))

        try:
            result = execute_query(query, args)
        except Exception as e:
            self._rollback_to(self.index-1)
            self.saved_queries[-1] = None
            raise e

        if get_query_verb(query) == 'select':
            self.index -= 1
            self._rollback_to(self.index)
            self.saved_queries.pop()

        return result


def execute_query(query, args = ()):
    return init_db().db.execute(query, args)


def indent(text):
    return "\t" + text.replace("\n","\n\t")


def get_query_verb(query):
    if query.startswith('\\'):
        return 'client'
    return re.split(r'\s', query, maxsplit=1)[0].lower()


def print_table_list(transaction_stack):
    sql = """
    SELECT c.table_name, COUNT(*) AS columns
    FROM INFORMATION_SCHEMA.COLUMNS c
    JOIN INFORMATION_SCHEMA.TABLES t ON t.table_schema='public' AND t.table_name=c.table_name AND t.table_type = 'BASE TABLE'
    WHERE c.table_schema='public'
    GROUP BY c.table_name
    """
    cursor = transaction_stack.run(sql, allow_cache=False, log=False)
    print_table([['table', 'columns']]+[list(row) for row in cursor], Commands.term_width)


def print_table_info(table_name, transaction_stack):
    sql = """
    SELECT c.*
    FROM INFORMATION_SCHEMA.COLUMNS c
    JOIN INFORMATION_SCHEMA.TABLES t ON t.table_schema='public' AND t.table_name=c.table_name AND t.table_type = 'BASE TABLE'
    WHERE c.table_schema='public' and c.table_name=?
    ORDER BY c.ordinal_position
    """
    columns = []
    for row in transaction_stack.run(sql, allow_cache=False, log=False, args=(table_name,)):
        columns.append([
            row['column_name'],
            row['data_type'] + (f"({row['character_maximum_length']})" if row['character_maximum_length'] != None else "") + ("" if row['is_nullable'] else " NOT NULL" ),
            row['column_default']
        ])
    if not columns:
        print(f'{COLOR_RED}No such table "{table_name}"{COLOR_RESET}')
        return
    print_table([['column', 'type', 'default']]+columns, Commands.term_width)

    footnotes = []
    constraints = set()
    sql = """
    SELECT
        con.conname constraint_name,
        pg_catalog.pg_get_constraintdef(con.oid, true) constraint_info
    FROM pg_constraint con
    JOIN pg_catalog.pg_class c ON c.oid = con.conrelid
    WHERE c.relnamespace='public'::regnamespace AND c.relname = ?

    """
    for row in transaction_stack.run(sql, allow_cache=False, log=False, args=(table_name,)):
        footnotes.append(f'* {row["constraint_info"]} {COLOR_DIM}"{row["constraint_name"]}"{COLOR_RESET}')
        constraints.add(row['constraint_name'])

    sql = """
    SELECT
        c2.relname index_name,
        pg_catalog.pg_get_indexdef(i.indexrelid, 0, true) index_info
    FROM pg_catalog.pg_index i
    JOIN pg_catalog.pg_class c ON c.oid = i.indrelid
    JOIN pg_catalog.pg_class c2 ON c2.oid = i.indexrelid
    WHERE c.relnamespace='public'::regnamespace and c.relname = ?
    """
    for row in transaction_stack.run(sql, allow_cache=False, log=False, args=(table_name,)):
        if row['index_name'] not in constraints:
            info = (" "+row['index_info']+" ") \
                .replace(f" INDEX {row['index_name']} ", " INDEX ") \
                .replace(f" CREATE ", " ") \
                .replace(f" ON {table_name} ", " ") \
                .replace(f" USING btree ", " ") \
                .strip()
            footnotes.append(f'* {info} {COLOR_DIM}"{row["index_name"]}"{COLOR_RESET}')

    if footnotes:
        print("\n" + "\n".join(footnotes))



def run_client_command(command, transaction_stack):
    print(f"{COLOR_BRIGHT}\\{command}{COLOR_RESET}\n")
    command = command.rstrip(';').split(' ')

    if command[0] in ('d', 'dt'):
        if len(command) == 1:
            print_table_list(transaction_stack)
        elif len(command) == 2:
            print_table_info(command[1], transaction_stack)
        else:
            print(f"{COLOR_RED}Too many arguments{COLOR_RESET}")
    else:
        print(f"{COLOR_RED}Unknown client command '\\{command[0]}'{COLOR_RESET}")
    print()


def get_sql_block_results(sql, transaction_stack, expected=False, show_output=False):
    run_time = 0
    tresult = dict(query_count = 0, modified_row_count = 0, run_time = 0, table_count = 0, tables = [])
    qresult = {}

    for query in split_queries(sql):

        if query.startswith('\\'):
            run_client_command(query[1:], transaction_stack)
            continue

        # Perform the actual query
        start_time = time.time()
        cursor = transaction_stack.run(query, allow_cache=False)
        run_time = float(time.time() - start_time)
        tresult['run_time'] += run_time
        tresult['query_count'] += 1

        # This dict will contain results for a single query. We'll only return its value for the 
        # last query of the batch.
        qresult = {}

        # Process SELECT or RETURNING data
        if cursor.description:
            rows = [list(row) for row in cursor]
            qresult['row_count'] = len(rows)
            qresult['column_count'] = len(cursor.description or [])
            qresult['column_names'] = [item[0] for item in cursor.description]
            qresult['column_types'] = [init_db().type_map.get(item[1],"text") for item in cursor.description]

            for col_index, col_type in enumerate(qresult['column_types']):
                if col_type == 'numeric':
                    for row in rows:
                        if row[col_index] != None:
                            row[col_index] = round(float(row[col_index]), 4)

            if expected:
                ordered = "order by " in query.lower()
                if len(rows) >= 25:
                    if ordered:
                        qresult['first_results'] = [qresult['column_names']] + rows[0:10]
                elif ordered or len(rows) < 2:
                    qresult['results'] = [qresult['column_names']] + rows
                else:
                    qresult['unordered_results'] = [qresult['column_names']] + rows
            else:
                data = qresult['results'] = qresult['unordered_results'] = qresult['first_results'] = [qresult['column_names']] + rows

                if show_output:
                    print_table(data if len(data) <= 101 else data[0:51], Commands.term_width)
                    showing = "" if len(data) <= 101 else " (only first 50 are shown)"
                    print(f"\n==> {len(data)-1} row(s) in {run_time:.3f}s{showing}\n")
        else:
            if show_output:
                rows = f"affected {cursor.rowcount} row(s)" if cursor.rowcount >=0 else "done"
                print(f"==> {rows} in {run_time:.3f}s\n")

        # Count modified rows
        verb = get_query_verb(query)
        if cursor.rowcount > 0 and verb not in ('select','explain'):
            tresult['modified_row_count'] += cursor.rowcount

        # Do an EXPLAIN
        if verb in ("select", "insert", "update", "delete"):
            cursor = transaction_stack.run(f"EXPLAIN (FORMAT JSON) {query}", allow_cache=False, log=False)
            plan = cursor.fetchone()[0]
            if plan:
                # print(query, json.dumps(plan,indent=4))
                tables = sorted(search_plan(plan, "Relation Name"))
                tresult['tables'] += tables
                tresult['table_count'] += len(tables)

    qresult.update(tresult)
    return qresult


def print_table(data, term_width):
    col_lens = [0] * len(data[0])
    for row in data:
        for col_num, value in enumerate(row):
            col_lens[col_num] = max(len(str(value)), col_lens[col_num])

    target_width = max(term_width, len(col_lens) * 6+1)
    while True:
        current_width = sum(col_lens) + len(col_lens)*3 + 1
        if current_width <= target_width:
            break
        idx = col_lens.index(max(col_lens))
        col_lens[idx] = int(col_lens[idx] * 0.8)

    for row_num, row in enumerate(data):
        for col_num, value in enumerate(row):
            l = col_lens[col_num] + 1
            value = str("NULL" if value==None else value)[0:l]
            print(f"| {value:<{l}}", end="|\n" if col_num+1==len(row) else "")
        if row_num == 0:
            for col_num, value in enumerate(row):
                l = col_lens[col_num] + 2
                print(f"|{'-'*l}", end="|\n" if col_num+1==len(row) else "")


def merge_alt_data(data, alt_data, add_missing_sections=False):
    for section, alt_info in alt_data.items():
        if not alt_info:
            continue

        if section not in data:
            if not add_missing_sections:
                continue
            data[section] = {}
        info = data[section]

        if 'sql' not in info:
            info['sql'] = "TODO"

        for label, expected in alt_info.items():
            if label != 'sql':
                info[label] = expected



def _run_as_daemon(daemon_callback, keep_fds=set(), change_cwd=True):
    # Do the Unix double-fork magic; see Stevens's book "Advanced
    # Programming in the UNIX Environment" (Addison-Wesley) for details
    pid = os.fork()
    if pid > 0:
        # Return the original process
        return

    # Decouple from parent environment
    if change_cwd:
        os.chdir("/")
    os.setsid()
    os.umask(0)

    keep_fds = {fd if type(fd)==int else fd.fileno() for fd in keep_fds}
    for file in [sys.stdout, sys.stderr, sys.stdin]:
        try:
            if file.fileno() not in keep_fds:
                file.close()
        except:
            pass
    for fd in range(1024):
        if fd not in keep_fds:
            try:
                os.close(fd)
            except:
                pass

    # Do second fork
    pid = os.fork()
    if pid > 0:
        # Exit from second parent; print eventual PID before exiting
        sys.exit(0)

    try:
        daemon_callback()
    except Exception as e:
        # Make sure an exception is not caught by _run_as_daemon's caller
        print(traceback.format_exc(), file=sys.stderr, flush=True)
    sys.exit(0)


already_reset = False


def run_transaction_server(server, as_daemon=False):

    with open(PID_FILE, 'w') as file:
        file.write(str(os.getpid()))

    if as_daemon:
        sys.stdout = sys.stderr = log_fd = open(LOG_FILE, 'a', buffering=1)
    else:
        log_fd = sys.stderr

    print("Transaction server started accepting connections..")

    while True:
        try:
            conn, addr = server.accept()
        except TimeoutError:
            break

        options = json.loads(conn.makefile("r").readline())
        print("Request:", options)

        sys.stdout = sys.stderr = conn.makefile("w", buffering=1) # print() output should go to the client

        Commands.term_width = options['term_width']
        if Commands.transaction_stack == None:
            Commands.transaction_stack = TransactionStack()
        if Commands.transaction_stack.start():
            Commands.reset(True)

        func = getattr(Commands, options['cmd'])
        func(*options['args'])

        sys.stdout.close()
        conn.close()
        sys.stdout = sys.stderr = log_fd

        print("Request handled")

    print("Exiting transaction server due to timeout")
    os.remove(SOCKET_FILE)
    os.remove(PID_FILE)
    sys.exit(0)


def get_term_width():
    try:
        return os.get_terminal_size().columns
    except:
        return 1500


class Commands:

    transaction_stack = None
    term_width = None

    @staticmethod
    def client():
        """Run the PostgreSQL client."""
        Commands.reset(True)
        try:
            subprocess.run(["psql"], env=init_db().env)
        except FileNotFoundError:
            print("The psql client is missing.")
            sys.exit(1)

    @staticmethod
    def expected(specific_line=None):
        """Generate 'Expected' clauses based on the SQL blocks in ./assignment.md."""
        Commands.execute(specific_line, expected=True)
        
    @staticmethod
    def execute(specific_line=None, expected=False):
        """Run all SQL blocks in ./assignment.md, checking if any Expected-clauses are satisfied. When a line number is given, the output for the SQL block containing that line is shown (and no further SQL blocks are run)."""
        todos = []
        errors = []
        oks = []

        if specific_line != None:
            specific_line = int(specific_line)
        data = parse_md("assignment.md", up_to_line=specific_line)

        for alt_filename in ["_template/assignment.md", "_solution/assignment.md"]:
            try:
                merge_alt_data(data, parse_md(alt_filename), add_missing_sections = specific_line==None)
            except FileNotFoundError:
                pass

        last_section = list(data)[-1]

        for section, info in data.items():
            sql = data.get(section, {}).get('sql', '')
            if sql.lower() in ('','todo'):
                todos.append(section)
                continue

            perform_checks = specific_line==None or section==last_section
            show_output = specific_line!=None and section==last_section

            try:
                if not perform_checks:
                    queries = [query for query in split_queries(sql) if get_query_verb(query) not in ('select','explain','client')]
                    if queries:
                        print(f"\n{COLOR_MAGENTA}### {section}{COLOR_RESET}\n")
                        for query in queries:
                            Commands.transaction_stack.run(query)
                    continue
                print(f"\n{COLOR_CYAN}### {section}{COLOR_RESET}\n")
                result = get_sql_block_results(sql, Commands.transaction_stack, expected, show_output)
            except Exception as e:
                print(f"{COLOR_RED}SQL error:{COLOR_RESET}\n{indent(e.args[0])}", flush=True)
                errors.append(section)
                continue

            if expected:
                for key, val in result.items():
                    val = EXPECT_TYPES[key][1](val)
                    print(f"Expected {key.replace('_',' ')}: {val}")
            else:
                ok = True
                for label, expect in info.items():
                    if label == 'sql': continue
                    if label not in result:
                        err = "no data"
                    else:
                        err = EXPECT_TYPES[label][2](result[label], expect)
                    if err:
                        ok = False
                        print(f"Expected {label.replace('_',' ')} ==> {COLOR_RED}{err}{COLOR_RESET}\n")
                if info and 'sql' not in info:
                    print(f"Section '{section}' has no ```sql``` block\n")
                if ok:
                    oks.append(section)
                else:
                    errors.append(section)

        if specific_line == None and not expected:
            print()
            for label, items in {"OK": oks, "ERROR": errors, "TODO": todos}.items():
                if items:
                    print(f"{label}:", ", ".join(items))
        else:
            print("Done!")


    @staticmethod
    def run(*args):
        """Run the given command with the proper environment variables for PostgreSQLite"""
        Commands.reset(True)

        if not args:
            print("A command (and optionally arguments) must be specified.")
        else:
            subprocess.run(args, env=init_db().env)


    @classmethod
    def reset(cls, only_when_empty=False):
        """Reset to template database"""

        db = init_db().db
        has_tables = db.execute("select 1 from pg_tables where schemaname='public' limit 1").fetchone()

        global already_reset
        if (only_when_empty and has_tables) or already_reset:
            return
        already_reset = True

        if has_tables:
            print("Dropping all database objects...")
            # Kill any other connections, as they may have locks on our data objects.
            # From https://tableplus.com/blog/2018/04/postgresql-how-to-kill-all-other-active-connection-to-database.html
            db.execute("""
            SELECT pg_terminate_backend(pg_stat_activity.pid)
            FROM pg_stat_activity
            WHERE pid <> pg_backend_pid()
            """)
            for what in ['view', 'table', 'sequence']:
                for row in db.execute(f"select {what}name as name from pg_{what}s where schemaname='public'"):
                    db.execute(f"drop {what} {row['name']} cascade")
            db.commit()

        if os.path.exists("create_db.sql") or os.path.exists("create_db.py"):
            print("Creating template database...")

        if os.path.exists("create_db.sql"):
            try:
                result = subprocess.run(["psql", "-bq", "--command=\\set ON_ERROR_STOP true", "--file=create_db.sql"], env=init_db().env, stdout=subprocess.DEVNULL)
            except FileNotFoundError:
                print("Please install the 'psql' client (contained in the 'postgresql-libs' Manjaro package).")
                sys.exit(1)

            if result.returncode:
                sys.exit(1)

        if os.path.exists("create_db.py"):
            sys.path = ['.'] + sys.path
            import create_db
            random.seed(42)
            create_db.run(db)
            db.commit()
            random.seed()


    @staticmethod
    def transaction_server(as_daemon=False):
        """Run the server process (that executes SQL blocks using cached transaction savepoints) in the foreground. This is for debugging purposes. It is normally started automatically in the background."""

        print("Starting transaction server...")

        if os.path.exists(SOCKET_FILE):
            os.remove(SOCKET_FILE)

        server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        server.bind(SOCKET_FILE)
        server.settimeout(2*60*60) # 2 hour timeout
        server.listen(1)

        if as_daemon:
            close_db()
            _run_as_daemon(lambda: run_transaction_server(server, as_daemon=True), keep_fds={server}, change_cwd=False)
        else:
            run_transaction_server(server)


    @staticmethod
    def stop_transaction_server():
        """Kill the currently running transaction server, if any."""

        try:
            with open(PID_FILE, "r") as file:
                pid = int(file.read())
            os.remove(PID_FILE)
        except (FileNotFoundError, PermissionError):
            return

        print(f"SIGTERM to {pid}...")
        try:
            os.kill(pid, 7)
        except (PermissionError, ProcessLookupError):
            return
        except ChildProcessError:
            pass

        for _ in range(50):
            try:
                killed_pid, status = os.waitpid(pid, os.WNOHANG)
                if killed_pid > 0:
                    break
            except ChildProcessError:
                break
        else:
            print(f"SIGKILL to {pid}...")
            os.kill(pid, 9)


def run_in_transaction_server(cmd, args):
    """Run all SQL blocks in ./assignment.md, checking if any Expected-clauses are satisfied. When a line number is given,
    the output for the SQL block containing that line is shown (and no further SQL blocks are run)."""

    client = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    connected = False
    try:
        client.connect(SOCKET_FILE)
        connected = True
    except (FileNotFoundError, ConnectionRefusedError):
        pass

    if not connected:
        Commands.transaction_server(as_daemon=True)
        client.connect(SOCKET_FILE)

    client.send((json.dumps({
        "cmd": cmd,
        "args": args,
        "term_width": get_term_width(),
    })+"\n").encode('utf8'))

    while True:
        out = client.recv(1024)
        if not out: break
        print(out.decode('utf-8'), end='', flush=True)
    
    client.close()


if __name__ == '__main__':
    cmd = sys.argv[1] if len(sys.argv)>1 else ""
    func = getattr(Commands, cmd, '')
    if func:
        if os.path.exists(".dbtool-ts.enabled") and cmd not in ('run','client','stop_transaction_server'):
            run_in_transaction_server(cmd, sys.argv[2:])
        else:
            Commands.term_width = get_term_width()
            Commands.transaction_stack = TransactionStack()
            if Commands.transaction_stack.start():
                Commands.reset(True)

            Commands.stop_transaction_server()
            func(*sys.argv[2:])
    else:
        print(f"Invalid command '{cmd}'. Commands are:")
        for name in dir(Commands):
            if not name.startswith('__'):
                func = getattr(Commands, name)
                if func.__doc__:
                    print(f"  {name} ==> {func.__doc__}")
        sys.exit(1)
