Functional requirements:
  _: |
    Create a new page!

    The page should contain the following elements, from top to bottom:

  Header:
    ^merge: feature
    code: False
    text: "It should contain the logo as well as links to other pages (that you don't need to create) for the different news sections: home, technology, business and science. When hovering any of the links, it should change its background color with a 1 second transition. Also, each of the links should have a suitable vector-based (so no png or jpg) icons. "
    text: |
      The header at the top of the page should..
      - Have a gradient background.
      - Show the logo (`logo.svg`).
      - Show links to other pages (that you don't need to create) for the different news section: home, technology, business and science.
        - When hovering any of the links, it should change its background color with a 1 second transition.
        - Each of the links should have a suitable vector-based (so no png or jpg) icon.
        - Clicking the link should have the browser try to open a non-existing file in the same directory as the html-file you created.
    map:
      html: 3
      css: 4
  Article:
    ^merge: feature
    code: False
    text: |
      Underneath the header comes the article itself. You can copy the contents from any news site you like or just use *lorem ipsum*. The article has:
      - a title,
      - an author,
      - an emphasized (usually **bold**) introduction,
      - several normal paragraphs of text,
      - at least two photos that take the full width of the article, each having a caption.
    map:
      html: 2
      css: 2  
  Comments:
    ^merge: feature
    code: False
    text: |
      Underneath the article there should be a comments section. At least two comments should be shown (commenter name, date and time, and of course the comment itself). And under these, there should be a form meant to submit new comments. It should have these fields:
      - Commenter name.
      - Comment (multiple lines of text entry should be possible).
      - An email address for confirmation.
      - A checkbox that promises the commenter's first born son to the news company (or something similarly evil). 
      - A way to select the continent you are posting from.
      - A submit button (that doesn't need to do anything).
    map:
      htmlforms: 2
      html: 2
      css: 2
  Side bar:
    ^merge: feature
    code: False
    map:
      html: 2
      css: 2
    text: To the right of the article and the comments is a side bar that takes about half the width of the article itself. It should contain at least three links to actual articles on other web sites. Each link has a title (preferably [clickbait](https://www.searchenginejournal.com/12-surprising-examples-of-clickbait-headlines-that-work/362688/), of course 😀) and a vaguely related image underneath it. On narrow screens, the side bar should be shown underneath the article.
  
Non-functional requirements:
  Typography:
    ^merge: feature
    code: False
    text: "At least one non-system font should be used."
    map: icons-fonts
  Graphical design:
    text: "The site's appearance is in accordance with basic graphical design guidelines: consistent, deliberate and beautiful use of colors, typography and margins."
    map: design
    0: A mess.
    1: Usability suffers due to incorrect use of colors, typography and margins.
    2: Very basic, but mostly consistent and still usable.
    3: Consistent and deliberate use of colors, typography and margins.
    4: Consistent, deliberate and beautiful use of colors, typography and margins.
  Responsive design:
    map: responsive
    text: "The site should adapt to look and work great on any browser width between 320px and 1920px. There should be at least two cut-off points, where something in the layout really changes. Between those, the interface can just scale with the viewport width. Double tapping on mobile should *not* zoom the page."
    0: "The site is not at all suitable for small or large browser widths."
    2: "The site works on all widths, but doesn't look professional on some. Acceptable quality for a local bakery website."
    3: "The site works well on any width, but there are a few visual compromises on some."
    4: "Perfect. Like you'd expect on an Apple product page."
  Valid HTML:
    text: Your page should pass the W3 HTML validator without issues.
    map:
      html: 2
      htmlforms: 1
    malus: 0.75
    0: Many types of errors.
    1: At most 2 different types of errors.
    2: Many types of warnings, no errors.
    3: At most 2 different types of warnings.
    4: No errors nor warnings.
  Semantic HTML:
    text: Where possible *semantic* HTML tags should be used.
    map:
      html: 1
    malus: 0.5
    0: 
    1: "At least 2 of the following tags should be used correctly: `header`, `form`, `section`, `nav`, `h?`, `p`."
    4: "At least 5 of the following tags should be used correctly: `header`, `form`, `section`, `nav`, `h?`, `p`."
  HTML indentation:
    text: The HTML should be indented consistently for readability.
    map:
      html: 1
    malus: 0.75
    0: No (or apparently random) indenting.
    1: Understandability suffers due to flawed or absent indenting.
    2: A bit sloppy here and there, but generally still quite easy to understand.
    4: Flawless. A few elemenents (such as `html` and `body`) may not have indenting (to prevent too deep indent), as long as it's done consistently.
  CSS readability:
    ^merge: codequality
    map:
      css: 3
    malus: 0.75
    text: |
      CSS is not only meant to be read by computers, but by humans as well. Your coworkers, and perhaps you yourself, a couple of months from now.

      That's why 'code quality' is important when writing CSS. Your CSS should be:
      - Indented consistently.
      - Well-organized, keeping relevant parts together.
      - Robust. Small changes in text lengths (translations, for example) should not cause the page to look wrong.
