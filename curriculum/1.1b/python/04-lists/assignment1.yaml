- |
    There are four collection data types in the Python programming language:
        1. _List_ is a collection which is ordered and changeable. Allows duplicate members.
        2. _Tuple_ is a collection which is ordered and unchangeable. Allows duplicate members.
        3. _Set_ is a collection which is unordered and unindexed. No duplicate members.
        4. _Dictionary_ is a collection which is unordered, changeable and indexed. No duplicate members.

    Today we will be focussing on lists (the other data types will be discussed later in the curriculum). Our task for today is to implement the game of Tic Tac Toe.

-   Features:
    -   Setup the game:
        -
            text: |
                ```
                *** Welcome to Tic Tac Toe ***
                Fill in the name of player 1: Timothy
                Fill in the name of player 2: Frank
                These are the players: ['Timothy', 'Frank']
                These are their markers: ['X', 'O']

                The game board looks like this:
                1, 2, 3, 4, 5, 6, 7, 8, 9,
                ```

                - The application asks the user to fill in the name of the first player and stores it in a list.
                - The application asks the user to fill in the name of the second player and stores it in a list.
                - The application maintains three lists for the player names, the player's markers and the gameboard.
            ^merge: feature
            
    -   Create a game loop:
        -
            text: |
                ```
                *** Welcome to Tic Tac Toe ***
                Fill in the name of player 1: Timothy
                Fill in the name of player 2: Frank
                These are the players: ['Timothy', 'Frank']
                These are their markers: ['X', 'O']

                The game board looks like this:
                1, 2, 3, 4, 5, 6, 7, 8, 9,

                Timothy, please choose a number on the board: A
                ERROR: Selection is not a number.
                Timothy, please choose a number on the board: 11
                ERROR: Number is not between 1 and 10.
                Timothy, please choose a number on the board: 1

                The game board looks like this:
                X, 2, 3, 4, 5, 6, 7, 8, 9,

                Frank, please choose a number on the board: 1
                ERROR: That square has already been taken.
                Frank, please choose a number on the board:
                ```

                - The application asks the user to choose a number from the gameboard.
                - The application validates the selection made by the user. A selection is valid when:
                  * The selection is a number.
                  * The selection (number) is between 1 (inclusive) and 9 (inclusive).
                  * The selected number has not already been chosen.
                - If a valid selection has been made then the game board (list) is updated accordingly and the turn is switched to the next player.
            ^merge: feature
            
    -   Create a nice looking gameboard:
        -
            text: |
                ```
                *** Welcome to Tic Tac Toe ***
                Fill in the name of player 1: Timothy
                Fill in the name of player 2: Frank
                These are the players: ['Timothy', 'Frank']
                These are their markers: ['X', 'O']

                The game board looks like this:
                 -----------
                | 1 | 2 | 3 |
                 -----------
                | 4 | 5 | 6 |
                 -----------
                | 7 | 8 | 9 |
                 -----------

                Timothy, please choose a number on the board: _
                ```

                - The application has a function that takes the current game board as a parameter and prints the game board as shown above. *Note:* function names usually take the forms of commands. Like `open_secret_chamber` or `launch_available_rockets`. Something like `game_board` would *not* be a good name for a function, as it doesn't tell us what should happen to the game board. (It might be a good name for a variable though.)
                - The application uses a `for` loop to print the game board. *Hint*: Use the `end` parameter of the `print` function to print the values within a single row without the cursor moving to the next line.
            ^merge: feature
            
    -   Check for winners:
        -
            text: |
                ```
                *** Welcome to Tic Tac Toe ***
                Fill in the name of player 1: Timothy
                Fill in the name of player 2: Frank
                These are the players: ['Timothy', 'Frank']
                These are their markers: ['X', 'O']
                ...
                ...
                ...
                Timothy, please choose a number on the board: 9

                The game board looks like this:
                 -----------
                | X | O | 3 |
                 -----------
                | 4 | X | O |
                 -----------
                | 7 | 8 | X |
                 -----------

                We have a winner! Congratulations Timothy!
                ```

                - The application computes the winner (using a function) after every valid turn.
                - A player has won when a row, column or diagonal contains the same values.
                - When a player has won the name of that player is displayed.

                *Hint:* create a function that checks if the user with a certain marker has won, and returns `True` if that is the case or `False` otherwise. There are 8 possible win conditions - it's okay to just create 8 `if`` statements to check them.
            ^merge: feature
            
          
    -   Check for draw:
        -
            text: |
                ```
                *** Welcome to Tic Tac Toe ***
                Fill in the name of player 1: Timothy
                Fill in the name of player 2: Frank
                These are the players: ['Timothy', 'Frank']
                These are their markers: ['X', 'O']
                ...
                ...
                ...
                Timothy, please choose a number on the board: 8

                The game board looks like this:
                 -----------
                | X | O | X |
                 -----------
                | X | X | O |
                 -----------
                | O | X | O |
                 -----------

                We have a draw! Thank you for playing.
                ```

                - If there's no winner after 9 turns (when all squares have been filled), the game ends in a draw. 
                - In case of a draw, a message should be displayed and the application should exit.
            ^merge: feature
            weight: 0.5