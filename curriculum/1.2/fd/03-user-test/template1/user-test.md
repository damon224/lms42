# User test

## Existing design:

**todo: If you used wiretext in the previous assingment, then copy the wireframes to the `before` folder. If you used quant-ux, then provide a link here.**

## Research objectives:

**todo: write down what you want to learn from this user study**

* 



## Typical users:

**todo: provide a list of what should be true of a candidate for your user test for that candidate to count as a typical user**

* 

## Selected users:

**todo: ask two fellow students to be in you user test**

1.
2.

## Tasks

**todo: given you research goals, what should the participants in your test do?**


*Task 1*

context:

task description:


*Task 2*

context:

task description:


## Thinking aloud

User 1 - Task1:
   **todo: add observations**



## Conclusions from user test
**todo: draw conclusions about your research objective base on the observations from the tests**

*

## Design changes

**todo: use the conclusions from the user test and what your learned from the materials on human factors to inform a new design. List the changes made and why below**  

*











