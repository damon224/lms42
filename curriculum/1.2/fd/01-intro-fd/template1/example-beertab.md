# Beertab

In this example we will discuss the functional design of a beer tracking system for a student house. The students in the student house want to share the cost of their beer supply. Students can grab a beer from the fridge and buy beer and put it in the fridge. They would like a beer tracking system - named ***Beertab*** - to keep track of who is putting beer in and taking it out.

To explore what the beer tracking system should do we'll write down a set of rules and examples to specify the behavior of the proposed system. Then we'll draw a logical entity relationship diagram and sketch out the interface in a set of wire-frames. Together these models can be discussed with the students to find out if we understand each other. The wire-frames can be used in a user test to check the usability of the system.

## Example Mapping

A (user) story is simply a label for a conversation you need to have with the customer about a particular aspect of the application. In this example we will assume that there is only one user story we need to implement (*track beer*). 

So, how do we work out what we need to build for the students to track beer? We will use rules and examples (ideally these should be real ones, but we can all imagine some for this case). Examples should be specific, because these triggers questions about the details of real situations we need to learn about to understand what needs to be build. 

A rule is what can be implemented in software. Rules may already be given as part of the domain or context the application will be part of. Examples can be used in turn to check if everybody understands the rule and to validate the rule by looking for exceptions. Coming up with rules and examples goes back and forth as you talk about / think about the story. 

When you have found key examples, you have also made a great start with building automated tests that can check if you implementation has the behavior you expect. It also helps in setting realistic tasks for a user-test (which we will do in a later assignment). This way of specifying the desired behavior of the application is called Example Mapping.

### Story: Track beer

*Note: Rules should be very specific and about one thing. Do not worry about it being too long.*

**Rule 1:** The number of beers taken from the fridge is deducted from the account of the student that takes them

- *Note: Every example has a title. Starting the title with "The one where ..." (like every episode of the series Friends) can help to better specify the topic of the example.*

- **Example 1.1:** The one where Bob drinks some beers
    1. Bob has 15 beers in his account
    2. Bob registers that he takes 3 beers from the fridge
    3. Bob has 12 beers in his account

- *Note: A rule can have multiple examples. Write the rule followed by all the examples belonging to this rule (mind the numbering). Do not write down all the rules and then all the examples afterwards because it will be difficult to know which examples belong to which rules.*

- **Example 1.2:** The one where Alice has drunk more than she bought
    1. Alice has -9 beers in her account
    2. Alice registers that she takes 2 beers from the fridge
    3. Alice has -11 beers in her account

**Rule 2:** The number of beers put in the fridge is added to the account of the student puts them in

- **Example 2.1:** The one where Bob buys some beers
    1. Bob has 15 beers in his account
    2. Bob registers that he puts 24 beers in the fridge
    3. Bob has 39 beers in his account

- **Example 2.2:** The one where Alice had to buy some beers
    1. Alice has -9 beers in her account
    2. Alice registers that she puts 48 beers in the fridge
    3. Alice has 39 beers in her account

*Note: We can explicitly model the behaviour of the 'thing' we are going to build by referring to it as 'the system' (because at this point in the design process we do not know yet what kind of application - mobile app, web app or an embedded system - we are going to build).*

**Rule 3:** The system displays the number of beers that should be in the fridge

- **Example 3.1:** The one where the amount of beers were taken is displayed
    1. The system displays that there are 31 beers in the fridge
    2. Bob registers that he takes 3 beers from the fridge
    3. The system displays that there are 28 beers in the fridge

- **Example 3.2:** The one where the amount of beers added is displayed
    1. The system displays that there are 31 beers in the fridge
    2. Bob registers that he puts 5 beers in the fridge
    3. The system displays that there are 36 beers in the fridge

**Rule 4:** The system displays a warning when the number of beers in the fridge falls below 12 and asks the student with the lowest number of beers in their account to get new beer

- **Example 4.1:** The one where the amount of beer goes below critical levels
    1. Bob has 5 beers in his account
    2. Alice has 10 beers in her account
    3. The system displays that there are 13 beers in the fridge
    4. Alice registers that she takes 2 beers from the fridge
    5. The system displays that there are 11 beers in the fridge
    6. The system displays a warning that Bob needs to go and get some more beer

- **Example 4.2:** The one where the beer supply restored
    1. The system displays that there are 11 beers in the fridge
    2. The system displays a warning that Bob needs to go and get some more beer
    3. Bob registers that he puts 24 beers in the fridge
    4. The system displays that there are 35 beers in the fridge
    5. The system does not display the warning anymore


## *Reflection*
Take a moment to reflect on the rules and examples above. Try to answer the following questions:
- Are there any rules missing? (if so, which ones)
    * ...
- Can you spot the *context*, *action*, *expected outcome* in Example 1.1? 
    * Context: ...
    * Action: ...
    * Expected outcome: ...
- Can you spot the *context*, *action*, *expected outcome* in Example 4.2? 
    * Context: ...
    * Action: ...
    * Expected outcome: ...
- What questions can you come up with based on these rules and examples? 
    * ...

## Datamodel

Based on the case description above we can create the following domain model.

```plantuml
' Some settings to make your ERD look better
hide circle
skinparam linetype ortho

entity student {
    * name: text
    profile_picture: text
}

entity mutation {
    * timestamp: datetime
    * amount: integer
}

entity house {
    * name: text
}

' Add notes
note right of mutation
  When buying beer a mutation is created with:
  - <i>amount</i>: +24
  - <i>timestamp</i>: the day and time of the mutation (e.g. 10:23 22/11/2023)

  When drinking a beer a mutation is created with:
  - <i>amount</i>: -1
  - <i>timestamp</i>: the day and time of the mutation (e.g. 16:01 22/11/2023)

end note

student ||--o{ mutation : registers
student }|--|| house : lives in
```

## *Reflection*
Take a moment to reflect on the domain model above. Try to answer the following questions:
- Why is the fridge not modeled in the domain?
    * ...
- Where is the number of beer in a student account stored in this model?
    * ...
- How is the system going to display the number of beer in the fridge?
    * ...
- What is a drawback of modeling the system this way? 
    * ...


## Wireframes

Now that we know the rules of the system and the information we need to store in the system (see domain model) we can create the look and feel of the system. We will do so using wire-frames.

You can find the example wire-frames (created in wiretext) here: [example-beertab.wt](example-beertab.wt)