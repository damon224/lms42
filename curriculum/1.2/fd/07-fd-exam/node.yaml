name: UX Project
description: Create a complete functional design for an app idea you have invented.
days: 2
ects: 5
type: project
public_for_students: true
miller: sh
allow_ai: true
goals:
    interview:
        title: Effectively interview stakeholders with regard to requirements.
        apply: 1
    requirements:
        title: Translate business requirements to a functional design.
        apply: 4
    usertesting:
        title: Conduct user studies.
        apply: 1
    wireframing:
        title: Draw wireframes to rapidly describe user interfaces.
        apply: 4
    datamodeling: 
        apply: 2
    device-design: 
        title: Create UX designs for different device types.
        apply: 1

assignment:
  - |
      In this module you have practiced with the process of getting from an idea for an application (improvement) to a validated set of specifications for implementing that application. These specifications include:

      * a statement of the job(s) someone wants to get done in a particular situation using the application and the tasks involved
      * the acceptance criteria for the behaviour of the system in terms of example maps (the rules and examples)
      * a domain model represented by a logical entity relationship diagram
      * user flows that map out how to complete the tasks (including the relation between actions and screens)
      * low fidelity wireframes

      In this final project you to go through the whole process once more, validate the prototype in a user test and use your findings to improve the design.

  - Jobs and tasks:
    -  
      text: |
        In the previous assignment you have specified an idea you want convert into a functional design. Copy the *title*, *job stories* and *tasks* from your idea into the provided `project.md`.

        *Note*: You are allowed to deviate from your idea if you have new insights during the project.
      must: true

  - Example mapping:
    -  
      text: |
        Pick **three** tasks from the tasks you have specified in the previous objective and work them out into rules and examples.

        Requirements for the tasks:
        - They should be about the core of your application (so no *login* or *user management* tasks)
        - They should contain some complex behaviour (so no *add housemate to house* tasks)
        - They should not be too similar (so no combination of *find for date* and *find for breeding* tasks)
        
      0: Example maps are not about the core functionality or incomplete. Rules are not a clear statement about the behaviour or state of the application that can be true or false. Examples are not relevant to the rule or are do not provide the right amount of detail to validate the rule.
      2: Example maps are mostly complete and relevant. Rules are clear, but examples are not concrete or specific enough
      4: Example maps are complete and about the core functionality. Rules are clear statements that must be true for the app to be acceptated as implementing the expected behaviour. The examples provide validation of the rules.
      map:
        requirements: 2

  - Formulate questions:
    -
      text: |
        Time to reflect on the rules you have specified. Write down any questions that came up during example mapping and any answers / decisions you made in response.

      0: Context, motivation and desired outcome are not explicit. Tasks are not relevant for the solution and not complete.
      4: Context, motivation and desired outcome are explicit and rich in detail. Tasks are relevant for the solution and complete.
      map:
        interview: 1

  - Domain model:
    - 
      text: |
        Provide a logical entity relationship diagram for the application with labels on the relations.
      0: Domain model does not give a clear impression of the application to be build
      4: Domain contains all entities with proper relationship (including correct cardinality).
      map:
        datamodeling: 2 

  - User flows:
    -
      text: |
        Create user flows for the tasks you have picked for the example mapping (listed in the second objective). Do not forget to label each flow (add a title).

        *Note* that there may be sub-tasks involved. We want to the user flows of the main task.

      0: Does not use the start, stop, decision, action, step/screen elements appropriately.
      2: Diagrams follow the format, but are not clear about the sequence of steps taken or the actions and decisions are not explicit.
      3: User flows are clear and explicit, but not complete
      4: All tasks have been cleary mapped out with explicit steps, actions and decision points.
      map:
        requirements: 2

  - Wireframes:
    -
      text: |
        Create low fidelity wireframes for all of the screens in your **mobile** application. Navigating your wireframes should not lead to dead ends.
        
        You can use either wiretext or quant-ux. 
        
        *Note*: The relationship between your user flows and the wireframes should be apparent. The wireframes should contain an implementation of the user flows you have created in the previous objective.
        
      0: Wireframes do not give a clear impression of the application to be build
      4: Clear wireframes including all the interaction elements required
      map:
        wireframing: 3

  - Redesign for desktop:
    -
      text: |
        In order to get an impression of how the app would look as a desktop application redesign it for larger screens.

        Your design should:
        - Contain a wireframe of the home page that links to all major parts of your application
        - Contain a wireframe for the first screan of each major part of your application (in other words the first level of navigation)

      0: users cannot navigate around to application to complete all the tasks
      3: users can navigate through all the flows without resetting the prototype or navigating outside the prototype
      4: users can navigate all flows and decision points are implemented as well.
      map:
        wireframing: 1
        device-design: 1

  - User testing:
    -
      text: |
        Test your low fidelity prototype (for the mobile application) with **two users** using the thinking aloud protocol. 
        
        - First define your research objectives. What would you like to learn about your application?
        - Then define your typical users by providing a list of what should be true of a candidate for your user test in order for that candidate to count as a typical user.
        - Define two tasks for the participants that will allow you to test what you have defined as your research goals.
        - Find two students to participate in you user test, ask them to perform the two tasks and make sure they think aloud. Record the user test session audio and screen capture. There are usb microphones available. Also list any observations you made.
        - Finally draw you conclusions about the user test with regard to the research goals and write them down.

      0: research goals, tasks and typical users are not clear or relevant. No thinking aloud happend
      3: research goals, tasks and typical users are clear, relevant and match each other. Thinking aloud session not really focussed or vocalised. Conclusion do not provide a clear path to improvement.
      4: research goals, tasks and typical users are clear, relevant and match each other. Test session demonstrates thinking aloud. Conclusions are relevant and will improve application.   
      map:
        usertesting: 1
