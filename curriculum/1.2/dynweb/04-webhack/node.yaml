name: Hacking
goals:
    web-security: 1
    auth: 1
days: 2
assignment:
    Assignment: |
        Although the provided web app, *Quoter XP*, seems to work just fine, it is riddled with security problems. Your task is to explain how they could be exploited and to fix them, in accordance with best practices. You may only change things when there is a valid security reason to do so.

        Each of the objectives below refers to one type of security problem that is present in the app. Good luck!

    Running the web app: |
        ```sh
        poetry install
        poetry run postgresqlite < schema.sql
        poetry run flask run --reload
        ```

    SQL injection:
        -
            link: https://www.w3schools.com/sql/sql_injection.asp
            title: W3Schools - SQL injection
            info: The problem explained.
        - 
            link: https://realpython.com/prevent-python-sql-injection/#passing-safe-query-parameters
            title: Preventing SQL Injection Attacks With Python
            info: The solution explained.
        -
            link: https://xkcd.com/327/
            title: XKCD - Exploits of a Mom
            info: A short comic joking about SQL injection.
        -
            title: SQL injection exploit
            text: |
                Run the Flask application and log in to it using user `test` and password `1234`. Now click on any of the quotes. Next, make use of SQL injection to learn the password of user `frank` by posting a comment.

                Copy your comment text into `exploits.txt`.

                *Hint:* subquery and string concatenation!
            4: Correct
        -
            title: SQL injection fix
            0: Still vulnerable
            4: Robust fix
            text: |
                Fix all SQL injection problems in the application. Note that we want the text stored in our database to be in plain text, so without any form of escaping.


    Cookies poisoning:
        -
            link: https://overiq.com/flask-101/cookies-in-flask/#drawbacks-of-cookie
            title: Cookies in Flask
            info: What are cookies, how can they be used from Flask, and what are the drawbacks of using cookies? This text doesn't talk about exploits yet, but does a good job at explaining cookies in a practical way.
        -
            link: https://www.acunetix.com/blog/web-security-zone/what-is-cookie-poisoning/
            title: What Is Cookie Poisoning
            info: The *Client-Side Cookie Poisoning* section is of particular interest.
        -
            title: Cookie poisoning exploit
            4: Correct
            text: |
                Within JavaScript console of the developer tools of your browser, you can manually set a cookie for a website by typing something like:

                ```javascript
                document.cookie = 'name=value';
                ```

                Presuming you didn't already know his password, log in as user `frank` using cookie poisoning. Copy the line you typed into your JavaScript to the `exploits.txt` file.
        -
            title: Cookie poisoning fix
            0: Still vulnerable
            4: Robust fix
            text: |
                Make cookie poisoning impossible for this app.

                *Hint:* fixing this problem involves either securing the cookie against tampering somehow *or* (and that's a lot easier) using a solution built-in to Flask that we've already encountered last lesson.


    Handling passwords:
        -
            link: https://www.youtube.com/watch?v=cczlpiiu42M
            title: Passwords & hash functions (Simply Explained)
            info: A good explanation of how (not) to store passwords. You can stop watching at 5m48s, where he starts talking about layering the approaches.
        -
            link: https://www.geeksforgeeks.org/password-hashing-with-bcrypt-in-flask/
            title: Example use of the flask-bcrypt library
            info: If you choose to use this library, you can install it using `poetry add flask-bcrypt`.
        -
            link: https://blog.twitter.com/official/en_us/topics/company/2018/keeping-your-account-secure.html
            title: Twitter - Keeping your account secure
            info: A blog article by Twitter about a different type of mistake they made regarding password handling. They're in good company, as many (large) companies have made similar mistakes.
        -
            title: Fix plaintext passwords in the database
            weight: 2
            0: Still vulnerable
            4: Robust fix
            text: |
                Update the create user and login routes. *Hint:* in `schema.sql` there are versions of the provided users with bcrypted password, which you can comment in instead of the unencrypted passwords. Recreate the database schema afterwards.
                ```python
                poetry run postgresqlite
                ```
        -
            title: More plaintext passwords stored on disk
            text: |
                Passwords are not only stored in the database, but can in some (common) scenarios also end up in another file on disk. Read the code to see if you can spot where, and fix this (by replacing the password with a couple of stars).
            0: Still vulnerable
            4: Robust fix


    Cross-site scripting (XSS):
        -
            link: https://www.youtube.com/watch?v=ABwS2MIxFPQ
            title: CISSPAnswers - Stored XSS (Cross-site Scripting)
        -
            link: https://www.youtube.com/watch?v=yJSnggHSH1U
            title: CISSPAnswers - Reflected XSS (Cross-site Scripting)
        -
            link: https://security.openstack.org/guidelines/dg_cross-site-scripting-xss.html
            title: Escape user input to prevent XSS attacks
            info: What does CSRF exploitable code look like in Python, and how to fix it?
        -
            title: Stored XSS exploit
            4: Correct
            text: |
                Use XSS to post a comment that will cause the text <span style="font-weight: bold; color: red;">HACKED</span> to be shown right on top of the *Quoter XP* brand name in the top left corner, whenever someone is viewing the comments page for that quote.

                *Hint:* you can use the `style` attribute and any dirty CSS tricks you can come up with to position your text in the top left corner of the page. Hacking can be dirty!

                Next try to automatically redirect every viewer of that page to Disney. The JavaScript for that is: `location.href = 'https://disney.com';`.

                Copy the full text that you typed into the comment for both of these hacks into `exploits.txt`.

        -
            title: Reflected XSS exploit
            4: Correct
            text: |
                Create an URL that when you send it to me and I open it, I'll see the front page with the following login prompt (instead of the usual warning):

                <img src="very-safe.png">

                Note that when hacking, nobody cares about tidy code/HTML/CSS, as long as it gets the job done. Copy the URL to `exploits.txt`.

        -
            title: XSS fix
            weight: 2
            0: Still vulnerable
            4: Robust fix
            text: |
                Fix all instances of XSS (stored as well as reflected).


    Client-side only validation:
        -
            link: https://httpie.io/docs#usage
            info: |
                `httpie` is a command line tool that can be used to send arbitrary HTTP requests from the command line. It can be installed using *Add/Remove Software*.
        -
            title: Missing authentication checks exploit
            4: Correct
            text: |
                Although the website wants only logged in users to be able to post new quotes, an attacker can post new quotes and comments without even having access to a user account, by just asking nicely through HTTP. Add a quote using `httpie`. Copy the command you used into `exploits.txt`.
        -
            title: Missing authentication checks fix
            0: Still vulnerable
            4: Robust fix
            text: |
                Add authentication checks to all routes that need it.


    Cross-site Request Forgery (CSRF):
        -
            link: https://medium.com/@charithra/introduction-to-csrf-a329badfca49
            title: Introduction to CSRF
            info: CSRF is the most complex attack in this lesson. This text explains it, and a reliable and relatively easy way to mitigate it (Anti-CSRF Tokens), pretty well. Take your time to really understand what you read.
        -
            title: CSRF exploit
            4: Correct
            weight: 2
            text: |
                Create a very small and simple HTML file that shows just a single button saying *Free beer!*. When the user presses it, he or she will be taken to one of the quote comment pages on *Quoter XP* running on `localhost`, and will see that a comment stating `I got hacked. Only losers get hacked.` has just been posted under her or his name. This does require the user to be logged into *Quoter XP* before he/she visits the hacker's page.

                **Note**: Google Chrome currently has protection against CSRF attacks enabled by default. You should test your exploit using Firefox.

                Copy your HTML into `exploits.txt`.

                Note that it would be <a href="https://stackoverflow.com/a/26380651" target="_blank">pretty easy</a> to post the comment without taking the user to the *Quoter XP* page, and <a href="https://stackoverflow.com/a/12563397" target="_blank">just as easy</a> to post the comment without the user having to press a button. The user wouldn't even notice what happened in the background. Feel free to try it if you like! 
        -
            title: CSRF fix
            0: Still vulnerable
            4: Robust fix
            text: |
                Mitigate any CSRF attacks.

                Although there are all kinds of libraries available to help you do this, they can make things a bit difficult to understand at first. We recommend that you implement a solution yourself, in order to fully grasp what's going on. 
                
                *Hint:* Generate a secret string, the CSRF token, for each session. That token should be included as a hidden field in each form. Form posts should be rejected if the submitted CSRF token doesn't match the one associated with the session. 
    
    Exposed data:
        -
            title: Exposed data exploit
            4: Correct
            text: |
                There is a bug that allows anybody to download private files (e.g. `app.py` or the access log). It's actually just a matter of typing the right URL in the browser. Copy this URL into `exploits.txt`.

                *Hint:* take a look at how `style.css` is served.

        -
            title: Exposed data fix
            0: Still vulnerable
            4: Robust fix
            text: Fix the problem such that private files cannot be accessed.

    Unencrypted HTTP: |
        Besides all of the above problems, this website also doesn't use transfer encryption, HTTPS. Any web app running in production nowadays should really work through HTTPS. For future reference: you can set this up for free using <a href="https://letsencrypt.org/">Let's Encrypt</a>. For web apps running in a local development environment, encryption is usually not practical (and not necessary), so we'll skip this for now.

