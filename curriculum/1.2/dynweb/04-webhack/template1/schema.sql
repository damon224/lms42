-- To (re)create a database with this schema, use the following shell command:
-- poetry run postgresqlite < schema.sql 

DROP SCHEMA public CASCADE;
CREATE SCHEMA public;

CREATE TABLE quotes (id serial PRIMARY KEY, text text NOT NULL, attribution text);
CREATE UNIQUE INDEX quotesUniqueIndex ON quotes (text, attribution);
INSERT INTO quotes(text,attribution) VALUES('Common sense is the collection of prejudices acquired by age eighteen.','Albert Einstein');
INSERT INTO quotes(text,attribution) VALUES('Great minds discuss ideas; average minds discuss events; small minds discuss people.','Eleanor Roosevelt');
INSERT INTO quotes(text,attribution) VALUES('If you aren''t, at any given time, scandalized by code you wrote five or even three years ago, you''re not learning anywhere near enough.','Nick Black');
INSERT INTO quotes(text,attribution) VALUES('What one programmer can do in one month, two programmers can do in two months.','Fred Brooks');
INSERT INTO quotes(text,attribution) VALUES('Never trust a computer you can''t throw out a window.','Steve Wozniak');
INSERT INTO quotes(text,attribution) VALUES('Really, I''m not out to destroy Microsoft. That will just be a completely unintentional side effect.','Linus Torvalds');
INSERT INTO quotes(text,attribution) VALUES('I don''t want to rule the universe. I just think it could be more sensibly organised.','Eliezer Yudkowsky');
INSERT INTO quotes(text,attribution) VALUES('The things you own end up owning you.','Tyler Durden');
INSERT INTO quotes(text,attribution) VALUES('You complete me.','Jerry Maguire');

CREATE TABLE users (id serial PRIMARY KEY, name text NOT NULL UNIQUE, password text NOT NULL);

INSERT INTO users(name,password) VALUES('test','1234');
--INSERT INTO users(name,password) VALUES('test','$2b$12$SiYzvbEgyPXJ.TAe5jmcb.hiqCY30tl/pk9nO2jbsl7qqTBcY9RdO'); -- 1234
INSERT INTO users(name,password) VALUES('frank','himom');
--INSERT INTO users(name,password) VALUES('frank','$2a$12$jQ/cSKHYeE.TxVQhRLhCdOKdeWjBA/2NQUvgpwAjhxA6rtA0l38oa'); -- himom

CREATE TABLE comments (id serial PRIMARY KEY, quote_id int NOT NULL, user_id int NOT NULL, time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, text text NOT NULL);
CREATE INDEX commentsCompoundIndex ON comments (quote_id, id);
