name: Static & abstract classes
description: Tic Tac Toe Pro, OOP-style!
goals:
    static: 1
    abstraction: 1
pair: true
days: 2
intro:
- Static variables and methods:
    -
        link: https://realpython.com/instance-class-and-static-methods-demystified/
        title: Python's Instance, Class, and Static Methods Demystified
        info: In this tutorial I’ll help demystify what’s behind class methods, static methods, and regular instance methods.
    -
        link: https://radek.io/2011/07/21/static-variables-and-methods-in-python/
        title: Static variables and methods in Python
        info: Static means, that the member is on a class level rather on the instance level. Static variables exist only on class level and aren't instantiated. Static methods don't refer to any instance of the class and can be called outside of it. Let's have a look how to get some static from Python.
    - |
        ## Static attributes and methods in class diagrams
        Static attributes (instance variables) and methods can also be represented in class diagrams, using an underline:

        ```plantuml
        @startuml
        class SerialNumber {
            -{static} next_number: int
            -number: int
            -{static} get_next_number(): int
            +get_number(): int
        }
        ```

        In Python:

        ```python
        class SerialNumber:
            __next_number = 0

            @classmethod
            def __get_next_number(cls):
                cls.__next_number += 1
                return cls.__next_number

            def __init__(self):
                self.__number = self.__get_next_number()

            def get_number(self):
                return self.__number
        ```

- Abstract classes:
    -
        link: https://www.youtube.com/watch?v=UDmJGvM-OUw
        title: Abstract Class and Abstract Method in Python
        info: In this video you will see what an abstract class is and why we need it. An abstract class can be considered as a pattern for other classes.
    -   link: https://www.geeksforgeeks.org/abstract-classes-in-python/
        title: Abstract Classes in Python
        info: An abstract class can be considered as a blueprint for other classes. It allows you to create a set of methods that must be created within any child classes built from the abstract class.
    -   link: https://www.askpython.com/python/oops/abstraction-in-python
        title: Understanding Abstraction in Python
        info: Today in this tutorial, we are going to discuss the concept of Abstraction in Python for the Object-Oriented Programming approach.
    - |
        ## Abstract classes in class diagrams
        Class diagrams also allow classes and methods to be abstract, indicated by an **A** instead of a **C** next to the class name, and the italic font used in the method name.

        ```plantuml
        abstract class Shape {
            +{abstract} draw()
        }
        class Circle {
            +draw()
        }
        Shape <|-- Circle
        ```

        In Python:

        ```python
        from abc import ABC, abstractmethod

        class Shape(ABC):
            @abstractmethod
            def draw(self):
                pass

        class Circle(Shape):
            def draw(self):
                print("Circle!")
        ```

