import random
import math
from gamelib import SpriteEntity, LabelEntity, Game

class Bird(SpriteEntity):
    def __init__(self, game):
        super().__init__(game, 'bird.png', x=game.width/2, y=game.height/2)
        self.__y_velocity = 0
        self.__x_velocity = 0

    def update(self, dt):
        if self.game.is_game_over(): # The bird is dead.
            # Rotate it, move it to the right, and shrink it.
            self.x += 55*dt
            self.y -= 15*dt
            self.rotation += dt*500
            self.scale *= 0.7**dt
        else: # The bird is alive!
            # Update vertical velocity and position.
            if 'UP' in self.game.keys_down:
                self.__y_velocity += 15*dt
            else:
                self.__y_velocity -= 5*dt
            self.y += self.__y_velocity
            self.rotation = -2 * self.__y_velocity

            # Update horizontal velocity and position.
            if 'LEFT' in self.game.keys_down:
                self.__x_velocity -= 5*dt
            elif 'RIGHT' in self.game.keys_down:
                self.__x_velocity += 5*dt
            else:
                self.__x_velocity *= 0.5**dt
            self.x += self.__x_velocity

        # Vertical bounce if the bird hits the top or the bottom.
        if (self.top > self.game.height and self.__y_velocity > 0) or (self.bottom < 0 and self.__y_velocity < 0):
            self.__y_velocity *= -0.6

        # If the bird hits the left or the right of the screen, the game is lost.
        if self.left < 0 or self.right > self.game.width:
            self.game.loose()


class Pipe(SpriteEntity):
    def __init__(self, game):
        # Make half the pipes go behind the GameOver sign.
        layer = random.choice([1, 3])

        # Half the pipes go on top, the other half go on the bottom.
        if random.choice([True, False]):
            bottom = random.randrange(game.height*3//10, game.height*8//10)
            super().__init__(game, 'pipe-top.png', layer=layer, bottom=bottom, left=game.width)
        else:
            top = random.randrange(game.height*2//10, game.height*7//10)
            super().__init__(game, 'pipe-bottom.png', layer=layer, top=top, left=game.width)

    def update(self, dt):
        self.x -= 200*dt
        # If the bird collides with a pipe, the game is lost.
        if self.collides_with(self.game.bird, 12):
            self.game.loose()


class Background(SpriteEntity):
    def __init__(self, game):
        super().__init__(game, 'background.png', layer=-1, left=0, bottom=0)


class GameOver(SpriteEntity):
    def __init__(self, game):
        super().__init__(game, 'gameover.png', layer=2, x=game.width/2, y=game.height/2)


class Score(LabelEntity):
    def __init__(self, game):
        super().__init__(game, layer=4, opacity=170, font_size=20)
        self.__time = 0

    def update(self, dt):
        if not self.game.is_game_over():
            self.__time += dt*10
        self.text = str(math.floor(self.__time))
        self.right = self.game.width - 10
        self.top = self.game.height - 3


class SappyGame(Game):
    def __init__(self):
        super().__init__(900, 504)
        self.__next_wall_time = 0
        self.__dead = False

    def start(self):
        self.clear_entities()
        self.__playing = True
        Background(self)
        self.bird = Bird(self)
        Score(self)

    def loose(self):
        if self.__playing:
            self.__playing = False
            GameOver(self)
            self.play_sound('death.wav')

    def is_game_over(self):
        return not self.__playing

    def update(self, dt):
        # Spawn walls every 1.5 to 3 seconds at random.
        self.__next_wall_time -= dt
        if self.__next_wall_time < 0:
            self.__next_wall_time = 1.5 + 1.5 * random.random()
            Pipe(self)


SappyGame().run()
