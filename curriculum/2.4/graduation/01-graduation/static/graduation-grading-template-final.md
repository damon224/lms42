# Associate degree Software Development

## Final graduation grading form

This form should be completed by the second examiner during the committee's final grading discussion. It *will* be disclosed to the student.

- Date: ...
- Student name: ...
- Student id: ...
- First examiner: ...
- Second examiner: ...
- External expert: ... / ... (Name / Company)


## 1. Create functional designs

- Committee discussion:
    - (No need if the individual forms align well.)
    - ...
    - ...
- What was good?
    - ...
    - ...
- What could have been better?
    - ...
    - ...
- Quality grade: ... (1 - 10)
- Complexity multiplier: ... (0 - 1.2)
- Independence multiplier: ... (0 - 1.2)
- Final grade: ... (1 - 10)
- Justification: ... (Remove line if the final grade logically follows from the above.)


## 2. Create technical designs

- Committee discussion:
    - (No need if the individual forms align well.)
    - ...
    - ...
- What was good?
    - ...
    - ...
- What could have been better?
    - ...
    - ...
- Quality grade: ... (1 - 10)
- Complexity multiplier: ... (0 - 1.2)
- Independence multiplier: ... (0 - 1.2)
- Final grade: ... (1 - 10)
- Justification: ... (Remove line if the final grade logically follows from the above.)


## 3. Implement software

- Committee discussion:
    - (No need if the individual forms align well.)
    - ...
    - ...
- What was good?
    - ...
    - ...
- What could have been better?
    - ...
    - ...
- Project 1
    - Quality grade: ... (1 - 10)
    - Complexity multiplier: ... (0 - 1.2)
    - Independence multiplier: ... (0 - 1.2)
- Project 2
    - Quality grade: ... (1 - 10)
    - Complexity multiplier: ... (0 - 1.2)
    - Independence multiplier: ... (0 - 1.2)
- Final grade: ... (1 - 10)
- Justification: ... (Remove line if the final grade logically follows from the above by taking the average.)


## 4. Apply insight to solve problems

- Committee discussion:
    - (No need if the individual forms align well.)
    - ...
    - ...
- What was good?
    - ...
    - ...
- What could have been better?
    - ...
    - ...
- Quality grade: ... (1 - 10)
- Complexity multiplier: ... (0 - 1.2)
- Independence multiplier: ... (0 - 1.2)
- Final grade: ... (1 - 10)
- Justification: ... (Remove line if the final grade logically follows from the above.)


## 5. Proficiently use common technologies

- Committee discussion:
    - (No need if the individual forms align well.)
    - ...
    - ...
- What was good?
    - ...
    - ...
- What could have been better?
    - ...
    - ...
- Estimated expert weeks: ...
- Final grade: ... (1 - 10)
- Justification: ... (Remove line if the final grade logically follows from interpolating the lookup table.)


## 6. Quickly and independently learn to use new techniques

- Committee discussion:
    - (No need if the individual forms align well.)
    - ...
    - ...
- What was good?
    - ...
    - ...
- What could have been better?
    - ...
    - ...
- Complexity/extensity grade: ... (1 - 10)
- Novelty multiplier: ... (0 - 1.2)
- Independence multiplier: ... (0 - 1.2)
- Final grade: ... (1 - 10)
- Justification: ... (Remove line if the final grade logically follows from the above.)


## 7. Automate software testing and deployment

- Committee discussion:
    - (No need if the individual forms align well.)
    - ...
    - ...
- What was good?
    - ...
    - ...
- What could have been better?
    - ...
    - ...
- Automated testing grade: ... (1 - 10)
- Automated quality checks grade: ... (1 - 10)
- Containerization grade: ... (1 - 10)
- Continuous integration and/or deployment grade: ... (1 - 10)
- Final grade: ... (1 - 10)
- Justification: ... (Remove line if the final grade logically follows from the above by taking the average.)


## 8. Collaborate within a development team

- Committee discussion:
    - (No need if the individual forms align well.)
    - ...
    - ...
- What was good?
    - ...
    - ...
- What could have been better?
    - ...
    - ...
- Roles & process descriptions & table quality grade: ... (1 - 10)
- Recommendation value grade: ... (1 - 10)
- Final grade: ... (1 - 10)
- Justification: ... (Remove if the final grade logically follows from the above by taking the average.)


## Journal

[ ] The journal meets the criteria.


## Overall grade

If any of the eight partial grades is lower than 5.5, the lowest will be the overall grade. Otherwise, the rounded average will be the overall grade. In case the journal (which is a precondition) is found to be incomplete, no grade can be assigned.

- Final grade: ... (1 - 10)


## Agreement

The following parties agree with all of the above...

[ ] First examiner (mandatory)
[x] Second examiner (mandatory)
[ ] External expert

In case the external expert does not agree:
- His/her view: ...
- Response by the examiners: ...


## Next steps

The second examiner must..
- Email this form to the other committee members immediately after discussion finishes.
- Announce the grades to the student and the audience (or ask the first examiner to do this).
- Add the grades to the LMS. Ask consent.
- Upload the individual and final forms to the LMS. (Currently using `scp`, better procedure pending.)

The first examiner must..
- Provide LMS grading consent.
- Verify that the exam dossier is complete in LMS. (Currently using `scp`, better procedure pending.)
- Add the final grade to Bison.
- Send an email to bison.act@saxion.nl to request the diploma.
