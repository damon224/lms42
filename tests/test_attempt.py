from common import run_attempt
import unittest

def test_permission(client):
    client.login("student1")
    client.open('/curriculum/linux')
    client.open('/curriculum/vars')
    client.find("input", value="Request teacher approval").require()


def test_assignment(client):
    run_attempt(client, "linux", student_name="student1", start_anyway=False, start_approval=False, submit=True)


def test_pause_attempt(client):
    client.login("student1")
    run_attempt(client, "linux", start_anyway=False, start_approval=False, submit=True)

    # test pause
    client.open('/curriculum/html')
    client.find(text="The previous node hasn't been graded yet.").require()
    client.find("input", value="Start attempt anyway").submit()

    run_attempt(client, "vars", start_anyway=True, start_approval=False, submit=False)
    client.find(text="Attempt paused!").require()

    client.open('/curriculum/html')
    client.find(text="This assignment is on pause. Finish your current assignment to resume.").require()

    run_attempt(client, "vars", start=False, submit=True)

    client.open('/curriculum/html')
    client.find("input.button", value='Submit attempt').require()


def test_pause_twice(client):
    client.login("student1")
    run_attempt(client, "linux", start_anyway=False, start_approval=False, submit=False)
    run_attempt(client, "vars", start_anyway=False, start_approval=True, submit=False)

    client.open('/curriculum/queries')
    client.find(text="You are already working on another assignment and you already have a paused attempt.").require()


def test_pause_exam(client, _db):
    client.login("student1")
    run_attempt(client, "pwa-project", start_anyway=False, start_approval=True, submit=False)

    client.open('/curriculum/linux')
    client.find(text="You cannot start another assignment while you are working on an exam.").require()


def test_active_module_limit(client):
    client.login("student1")
    # Start, finish and grade the linux topic (should not count as active)
    run_attempt(client, "linux", submit=True, grade="passed")

    # Start 3 other modules
    run_attempt(client, "vars", submit=True, start_approval=False)
    run_attempt(client, "javascript-dom", submit=True, start_approval=True)
    run_attempt(client, "html", submit=True, start_approval=False)

    # Verify that we can't start another without approval
    client.open('/curriculum/classes')
    client.find(text="You're already working on 3 modules.").require()
    client.find("input", value="Request teacher approval").require()

    # Finish one of the 3 modules (by passing the exam)
    run_attempt(client, "pwa-project", submit=True, start_approval=True, grade='passed')

    # Check that we can now start another module without consent
    run_attempt(client, "intro-fd", submit=True, start_approval=False)

    # Test that we can start a fourth module with approval
    client.open('/curriculum/classes')
    client.find(text="You're already working on 3 modules.").require()
    run_attempt(client, "classes", submit=True, start_approval=True, grade='passed')

    # Test that we can start a second node within an active module
    run_attempt(client, "accessing", submit=True, start_approval=False)

    # Test that we cannot start a fifth module without approval
    client.open('/curriculum/flask1')
    client.find(text="You're already working on 4 modules.").require()


def test_retry_attempt(client):
    client.login("student1")
    run_attempt(client, "linux", submit=True, start_approval=False, start_anyway=False, grade="failed")
    run_attempt(client, "linux", submit=True, start_approval=False, start_anyway=False, grade="passed")


def test_retract_approval(client):
    client.login("student1")

    client.open("/curriculum/pwa-project")
    client.find("input", value="Request teacher approval").submit()
    client.find(text="Go find a teacher to approve the assignment!").require()

    client.find("input", value="Retract approval request").submit()

    client.open("/curriculum/pwa-project")
    client.find("input", value="Request teacher approval").require()

def test_ects_count(client):
    client.login("student1")

    # When a student has not done an exam they should have zero ECTS
    client.open("/people/student1")
    client.find(text="Current ECTS: 0").require()

    # When a student has done an exam they should have 5 ECTS
    for _ in range(2):
        run_attempt(client, "pwa-project", submit=True, start_approval=True, start_anyway=False, grade="passed")
        client.open("/people/student1")
        client.find(text="Current ECTS: 5").require()


    run_attempt(client, "android-project", submit=True, start_approval=True, start_anyway=False, grade="passed")
    client.open("/people/student1")
    client.find(text="Current ECTS: 10").require()
