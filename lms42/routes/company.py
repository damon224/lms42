"""Provides routes for the company overview system."""

import datetime
import os
import random
import string
import sys

import flask
from enum import Enum
from flask_login import current_user, login_required
from werkzeug.datastructures import FileStorage
# from ..models.user import User
from ..email import send as send_email

from ..app import app, db, scheduler
from ..forms.company import AddCompanyForm, EditCompanyForm, EditCompanyFormTeacher, CompanyContactForm
from ..forms.safari import AddSafariForm, AddSafariOfferForm
from ..models.company import Company, CompanyContact
from ..models.safari import Safari, SafariOffer
from ..utils import role_required
from sqlalchemy.sql.expression import func


LOGO_DIR = app.config['DATA_DIR'] + '/logos'
os.makedirs(LOGO_DIR, exist_ok=True)


class NotificationType(Enum):
    SCHEDULED = 1
    CANCELED = 2
    SIGNUP_CLOSED = 3


def assert_authorization(required_company_id=None):
    valid_teacher = current_user.is_authenticated and current_user.is_teacher
    if required_company_id != flask.session.get('company_id') and not valid_teacher:
        print("Company permission denied", required_company_id, flask.session.get('company_id'), valid_teacher)
        flask.abort(403)


def save_logo(company, form):
    """Save the logo from the form into the filesystem.

    Args:
        company (Company): The Company object that the logo belongs to.
        form (FlaskForm): The Form object that contains the file in its 'logo'.

    Returns:
        str: The filename that the logo was saved with.
    """

    if isinstance(form.logo.data, FileStorage):
        logo_file = form.logo.data
        rand_str = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
        filename = f'{rand_str}.png'

        logo_file.save(os.path.join(LOGO_DIR, filename))

        return filename

    return company.logo


@app.route('/companies', methods=['GET', 'POST'])
def show_companies():
    if current_user.is_authenticated:
        companies = Company.query.order_by(Company.name) # func.random())
        if not current_user.is_teacher:
            companies = companies.filter_by(company_publish=True, teacher_publish=True)
        print(str(companies))
    elif company_id := flask.session.get('company_id'):
        return flask.redirect(f'/companies/{company_id}')
    else:
        return flask.redirect("/user/login?next=/companies")

    safaris = Safari.query.order_by(Safari.first_date)
    return flask.render_template('companies.html', companies=companies, safaris=safaris)


@app.route('/companies/<int:company_id>', methods=['GET'])
def show_company(company_id):
    company = Company.query.get(company_id)
    if not company:
        flask.abort(404)

    is_admin = company.id == flask.session.get('company_id') or (current_user.is_authenticated and current_user.is_teacher)
    accessible = is_admin or (current_user.is_authenticated and company.company_publish and company.teacher_publish)
    if not accessible:
        return flask.redirect("/user/login?next=/companies")
    
    student_contact = any(contact.student_visible for contact in company.contacts)
    safari_contact = any(contact.safari_messages for contact in company.contacts)

    safaris = Safari.query.order_by(Safari.first_date) if is_admin else None

    return flask.render_template('company.html', company=company, is_admin=is_admin, student_contact=student_contact, safari_contact=safari_contact, safaris=safaris)


@app.route('/companies/add', methods=['GET', 'POST'])
@role_required('teacher')
def add_company():
    company_form = AddCompanyForm()

    if not company_form.validate_on_submit():
        return flask.render_template(
            'generic-form.html',
            form=company_form,
            title="Add a company"
        )

    company = Company()
    company_form.populate_obj(company)

    db.session.add(company)
    db.session.commit()

    return flask.redirect(flask.url_for('show_companies'))


@app.route('/companies/<int:company_id>/edit', methods=['GET', 'POST'])
def edit_company(company_id):
    assert_authorization(company_id)
    company = Company.query.get(company_id)

    is_teacher = current_user.is_authenticated and current_user.is_teacher
    company_form = EditCompanyFormTeacher(obj=company) if is_teacher else EditCompanyForm(obj=company)

    if not company_form.validate_on_submit():
        return flask.render_template(
            'generic-form.html',
            title='Edit company info',
            form=company_form,
        )

    company_form.populate_obj(company)
    company.logo = save_logo(company, company_form)

    db.session.commit()

    return flask.redirect(
        flask.url_for(
            'show_company',
            company_id=company_id,
        ),
    )
     

@app.route('/companies/<int:company_id>/contacts/add', methods=['GET', 'POST'])
def add_company_contact(company_id):
    assert_authorization(company_id)

    contact_form = CompanyContactForm()
    if contact_form.validate_on_submit():
        contact = CompanyContact()
        contact_form.populate_obj(contact)
        contact.company_id = company_id
        db.session.add(contact)
        db.session.commit()
        return flask.redirect(f"/companies/{company_id}")

    company = Company.query.get(company_id)
    return flask.render_template('generic-form.html', form=contact_form, title=f"Add contact for {company.name}")


@app.route('/companies/<int:company_id>/contacts/<int:contact_id>/edit', methods=['GET', 'POST'])
def edit_company_contact(company_id, contact_id):
    assert_authorization(company_id)
    contact = CompanyContact.query.filter_by(company_id=company_id, id=contact_id).first()

    contact_form = CompanyContactForm(obj=contact)
    if contact_form.validate_on_submit():
        contact_form.populate_obj(contact)
        db.session.commit()
        return flask.redirect(f"/companies/{company_id}")

    company = Company.query.get(company_id)
    return flask.render_template('generic-form.html', form=contact_form, title=f"Edit contact for {company.name}")


@app.route('/companies/<int:company_id>/contacts/<int:contact_id>/delete', methods=['POST'])
def delete_company_contact(company_id, contact_id):
    assert_authorization(company_id)
    contact = CompanyContact.query.filter_by(company_id=company_id, id=contact_id).first()
    db.session.delete(contact)
    db.session.commit()
    return flask.redirect(f"/companies/{company_id}")


@app.route('/companies/<int:company_id>/delete', methods=['POST'])
@role_required('admin')
def delete_company(company_id):
    company = Company.query.get(company_id)

    db.session.delete(company)
    db.session.commit()

    flask.flash(f'{company.name} deleted.')

    return flask.redirect(flask.url_for('show_companies'))


@app.route('/companies/safaris/<int:safari_id>')
@login_required
@role_required('teacher')
def show_safari(safari_id):
    safari = Safari.query.get(safari_id)
    if not safari:
        flask.abort(404)

    return flask.render_template('safari.html', safari=safari)


@app.route('/companies/safaris/add', methods=['GET', 'POST'])
@role_required('admin')
def add_safari():
    add_safari_form = AddSafariForm()

    if not add_safari_form.validate_on_submit():
        return flask.render_template(
            'generic-form.html',
            form=add_safari_form,
        )

    safari = Safari()
    add_safari_form.populate_obj(safari)

    db.session.add(safari)
    db.session.commit()

    notify_companies_scheduled(safari)

    # scheduler.add_job(
    #     student_sign_up_closed_job,
    #     trigger='date',
    #     run_date=safari.student_signup_end_date + datetime.timedelta(days=1),
    #     id=f'notify_companies_{safari.id}',
    #     args=[safari.id]
    # )

    return flask.redirect(
        flask.url_for(
            'show_safari',
            safari_id=safari.id,
        ),
    )


@app.route('/companies/safaris/<int:safari_id>/delete', methods=['POST'])
@role_required('admin')
def delete_safari(safari_id):
    safari = Safari.query.get(safari_id)
    
    # scheduler.remove_job(f'notify_companies_{safari.id}')

    db.session.delete(safari)
    db.session.commit()

    notify_companies_cancel(safari)
    return flask.redirect(flask.url_for('show_companies'))


@app.route('/companies/<int:company_id>/safaris/<int:safari_id>', methods=['GET', 'POST'])
def show_safari_company(company_id, safari_id):
    assert_authorization(company_id)

    safari = Safari.query.get(safari_id)
    if not safari:
        flask.abort(404)

    company = Company.query.get(company_id)
    
    offer = safari.get_offer(company_id)
    form = AddSafariOfferForm(obj=offer)

    if not form.validate_on_submit():
        return flask.render_template(
            'safari-form.html',
            form=form,
            safari=safari,
            company=company
        )

    if not offer:
        offer = SafariOffer()
        offer.safari_id = safari.id
        offer.company = company
        db.session.add(offer)
    
    form.populate_obj(offer)
    db.session.commit()

    return flask.redirect(
        flask.url_for(
            'show_company',
            company_id=company.id
        ),
    )


# TODO: Remove, in favour of adding it to the company details page?
@app.route('/companies/<int:company_id>/safaris/<int:safari_id>/info', methods=['GET'])
@role_required('teacher')
def company_safari_info(company_id, safari_id):
    safari_company = SafariOffer.query.filter(SafariOffer.company_id == company_id, SafariOffer.safari_id == safari_id).first()
    safari = Safari.query.get(safari_id)

    if flask.request.args.get('is_student') == "yes":
        student_safari_day_id = get_student_safari_day_id(safari, current_user)

        return create_company_safari_info(safari_company, safari, current_user, student_safari_day_id)

    return create_company_safari_info(safari_company, safari)


@app.route('/companies/<int:company_id>/safaris/<int:safari_id>/add-student', methods=['POST'])
@login_required
def user_join_safari(company_id, safari_id):
    assert False
    company = Company.query.get(company_id)
    # TODO: add the actual user, instead of people signing up other people
    student = User.query.get(flask.request.args.get('student_id'))
    
    offer = SafariOffer.query.filter_by(company_id=company_id, safari_id=safari_id).first()

    # TODO: check if student counts remains within slot count
    offer.students.append(student)
    db.session.commit()

    # TODO: Ditch the htmx abuse?
    return create_htmx_refresh(f'Great! You have joined {company.name} for the safari day on {flask.request.args.get("date")}.')


@app.route('/companies/<int:company_id>/safaris/<int:safari_id>/remove-student', methods=['POST'])
@role_required('teacher')
def user_disjoin_safari(company_id, safari_id):
    assert False
    company = Company.query.get(company_id)
    # TODO: add the actual user, instead of people signing up other people
    student = User.query.get(flask.request.args.get('student_id'))
    
    safari_date = datetime.datetime.strptime(flask.request.args.get('date'), '%Y-%m-%d')
    offer = SafariOffer.query.filter_by(company_id=company_id, safari_id=safari_id).first()

    offer.students.remove(student)
    db.session.commit()

    # TODO: Ditch the htmx abuse?
    return create_htmx_refresh(f'You are no longer joining {company.name} for the safari day on {flask.request.args.get("date")}.')


# TODO: Shouldn't this be in Jinja? And I'd prefer to extend the company details page with this.
def create_company_safari_info(safari_company, safari, student = None, student_safari_day_id = None):
    assert False
    info = f"""<h2>Safari at {safari_company.company.name}</h2>
    <i>{safari_company.extra_information}</i></br></br>
    <b>Contact person</b>: {safari_company.contact_person}</br>
    <b>Reachable at</b>: {safari_company.contact_person_info}</br></br>"""

    if student:
        for safari_day in safari_company.safari_days:
            # Still in company sign up
            if safari.company_signup_open:
                info += f"Joining this safari is possible after <b>{safari.company_signup_end_date}</b>."
                break
            # Student sign up closed, no more joining or disjoining
            if not safari.student_signup_open():
                info += f"The student signup window ended on <b>{safari.student_signup_end_date}</b>."
                break
            if not student_safari_day_id:
                # Student is not yet in any safari
                if safari_day.has_available_spots:
                    info += f"""<button hx-post='{flask.url_for('user_join_safari', 
                            company_id=safari_company.company_id, 
                            safari_id=safari_company.safari_id, 
                            student_id=student.id, 
                            date=safari_day.date)}' hx-swap='none'>
                            Join on {safari_day.date}
                            </button></br>"""
                else:
                    info += f"<button disabled>Join on {safari_day.date}</button>"
            elif student_safari_day_id == safari_day.id:
                # Student is in this safari and may disjoin.
                info += f"""<button class='is-important' hx-post='{flask.url_for('user_disjoin_safari', 
                        company_id=safari_company.company_id, 
                        safari_id=safari_company.safari_id, 
                        student_id=student.id, 
                        date=safari_day.date)}' hx-swap='none'>
                        Disjoin {safari_day.date}
                        </button></br>"""
            else:
                # Student has already joined a different company for this safari
                info += f"<button disabled>Join on {safari_day.date}</button>"

    return info


# TODO: remove?
def get_student_safari_day_id(safari, student):
    matching_safari_day_id = next((safari_day.id for company in safari.companies
                                            for safari_day in company.safari_days
                                            for participant in safari_day.students
                                            if participant.id == student.id), None)

    return matching_safari_day_id


# TODO: this seems funky. Why use this over a redirect?
def create_htmx_refresh(message=None):
    refresh = flask.make_response()
    refresh.headers["HX-Refresh"] = "true"

    if message:
        flask.flash(message)

    return refresh


FOOTER = """Met vriendelijke groet,

Frank van Viegen
Teamleider en docent
Associate degree Software Development
f.c.vanviegen@saxion.nl | 06 5750 3659"""

EXPLAIN = """Voor wie de Business Safari nieuw is, of een geheugensteuntje kan gebruiken: Drie keer per jaar sturen wij (de tweejarige Software Development-opleiding van Saxion) onze studenten een dag 'het veld in'. We hopen dat jouw bedrijf een klein groepje studenten zou willen huisvesten. Minimale benodigdheden: een werkplek, wifi en een (junior) ontwikkelaar die beschikbaar wil zijn als gastdame of -heer. De studenten werken in principe aan hun eigen studie-opdrachten, maar krijgen ondertussen natuurlijk wel de ambiance en dagelijkse activiteiten van het bedrijf mee. Het liefst zien we de studenten daarom niet alleen met elkaar in een ruimte, maar ergens waar sfeer te proeven valt. Indien mogelijk zou het mooi zijn als de studenten in sommige activiteiten meegenomen kunnen worden. Denk aan het meekijken bij een stand-up of andere meeting, een rondleiding, een welkomstwoordje, gezamenlijk lunchen, etc. Ook wordt het erg gewaardeerd als de ontwikkelaars van het bedrijf interesse tonen in het werk van de student en wat willen laten zien van hun eigen werk. Maar er hoeft niks!"""

def get_names_and_emails(company):
    emails = []
    names = []
    for contact in company.safari_contacts:
        emails.append(contact.email)
        names.append(contact.first_name or contact.role or contact.last_name or company.name)

    if len(names) > 1:
        last = names.pop()
        names[-1] += " en " + last
    names = ', '.join(names)
    return names, emails


def notify_companies_scheduled(safari):
    title = "Business Safari in aantocht"

    for company in Company.query.all():
        names, emails = get_names_and_emails(company)
        if not emails:
            continue
        url = company.create_login_link()

        message = f"""Beste {names},

Met plezier kunnen we melden dat er weer een Business Safari aan zit te komen, op {safari.first_date} (en {safari.second_date})!

We hopen dat {company.name} een aantal van onze studenten wil ontvangen. Laat het alsjeblieft even weten via {url} vóór {safari.company_signup_end_date}. Na die datum kunnen studenten inschrijven en vervolgens horen jullie van ons wie je kunt verwachten. Het zou heel fijn zijn als je gelijk even de algemene bedrijfsinformatie (ook gebruikt voor onze stage-database) wil controleren en aanvullen.

{EXPLAIN}

Alvast heel erg bedankt voor jullie hulp!

{FOOTER}
"""
    
        send_email(emails, title, message, fake=False)


def notify_companies_remind(safari):
    title = "Reminder: Business Safari"

    for company in Company.query.all():
        names, emails = get_names_and_emails(company)
        offer = SafariOffer.query.filter_by(company_id=company.id, safari_id=safari.id).first()
        if offer or not emails:
            continue
        url = company.create_login_link()

        message = f"""Beste {names},

Onze Business Safari komt snel dichterbij ({safari.first_date} en {safari.second_date}), maar we hebben nog niet gehoord of jullie deze keer kunnen en willen deelnemen. Zou het lukken om dit vandaag of morgen nog even aan te geven? Dat kan via: {url} - Het zou heel fijn zijn als je gelijk even de algemene bedrijfsinformatie (ook gebruikt voor onze stage-database) wil controleren en aanvullen.

{EXPLAIN}

Alvast heel erg bedankt voor jullie hulp!

{FOOTER}
"""
    
        send_email(emails, title, message, fake=False)


def notify_companies_cancel(safari):
    title = "Business Safari geannuleerd"

    for company in Company.query.all():
        names, emails = get_names_and_emails(company)
        if not emails:
            continue
        message = f"""Beste {names},

Helaas kan de Business Safari van {safari.first_date} en en {safari.second_date} niet doorgaan. Sorry voor het ongemak!

Reply gerust mochten er vragen zijn naar aanleiding hiervan.

{FOOTER}
"""
        send_email(emails, title, message, fake=True)


def notify_companies_closed(safari):
    title = "Business Safari studenten"

    for offer in safari.offers:
        names, emails = get_names_and_emails(offer.company)
        if not emails:
            continue
        message = f"""Beste {names},

De inschrijving voor de Business Safari is zojuist gesloten.
"""

        date = safari.alt_date if offer.use_alt_day else safari.first_date
        if len(offer.students):
            message += f"\nOp {date} kunnen jullie de volgende student(en) verwachten:\n"

            for student in offer.students:
                message += f"- {student.first_name} {student.last_name} <{student.email}>\n"
            message += f"""
De student(en) hebben de volgende informatie ontvangen:

Contactpersoon: {offer.contact_person or '?'}, {offer.contact_person_info or '?'}
{offer.extra_information.strip()}

{EXPLAIN}

Reply gerust mochten er naar aanleiding hiervan vragen zijn. Alvast heel hartelijk bedankt, en wij wensen jullie een geslaagde safari-dag toe!

{FOOTER}
"""

        else:
            message += f"""
Helaas hebben zich voor {date} bij jullie geen studenten ingeschreven. Sorry, kan gebeuren. Volgende keer beter? Reply gerust mochten er naar aanleiding hiervan vragen zijn!

{FOOTER}
"""

        send_email(emails, title, message, fake=True)


# def student_sign_up_closed_job(safari_id):
#     safari = Safari.query.get(safari_id)

#     notify_companies_closed(safari)


if __name__ == '__main__':
    # bin/sd42 run python -m lms42.routes.company remind
    with app.app_context():
        if len(sys.argv) != 2:
            print("Command expected")
        elif sys.argv[1] == 'remind':
            safari = Safari.query.order_by(Safari.id.desc()).first()
            notify_companies_remind(safari)
        else:
            print("Invalid command")
