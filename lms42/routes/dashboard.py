import wtforms as wtf
import wtforms.validators as wtv
import flask_wtf
from flask_login import current_user
import flask
import re
from decimal import Decimal

from ..app import db, app
from ..utils import role_required
from ..models.dashboard import DashboardQuery
from ..models.user import User


class QueryForm(flask_wtf.FlaskForm):
    name = wtf.StringField('Name', validators=[wtv.DataRequired()])
    sql = wtf.TextAreaField('SQL')
    explanation = wtf.TextAreaField('Explanation')
    submit = wtf.SubmitField('Save')


def get_queries():
    return DashboardQuery.query.order_by(DashboardQuery.name)


@app.route('/dashboard', methods=['GET'], strict_slashes=False)
@role_required('teacher')
def dashboard():
    return flask.render_template('dashboard.html', queries=get_queries())


@app.route('/dashboard/new', methods=['GET', 'POST'])
@role_required('admin')
def dashboard_new():
    query_form = QueryForm()
    if query_form.validate_on_submit():
        query = DashboardQuery()
        query_form.populate_obj(query)
        db.session.add(query)
        db.session.commit()
        return flask.redirect(f'/dashboard/{query.id}')

    return flask.render_template('dashboard.html', queries=get_queries(), query_id='new', query_form=query_form)


@app.route('/dashboard/<int:id>', methods=['GET', 'POST'])
@role_required('teacher')
def dashboard_query(id):
    query = DashboardQuery.query.get(id)
    if not query:
        return flask.render_template('404.html', message='No such query')

    query_form = None
    if current_user.is_admin:
        query_form = QueryForm(obj=query)
        if query_form.validate_on_submit():
            if not query_form.sql.data.strip():
                DashboardQuery.query.filter_by(id=id).delete()
                db.session.commit()
                return flask.redirect(flask.url_for('dashboard'))
            query_form.populate_obj(query)
            db.session.commit()

    rows = None
    columns = None
    error = None
    graph = None
    users = None
    if query.sql.lstrip():
        try:
            db.session.execute('SET statement_timeout = 30000')
            db.session.execute('SET TRANSACTION READ ONLY')
            sql = query.sql.rstrip().rstrip(';')
            if not re.search(r"\slimit\s+\d+$", sql, re.IGNORECASE):
                sql += " limit 250"
            results = db.session.execute(sql)
            columns = list(results.keys())
            rows = list(results)
        except Exception as err:
            error = err
        db.session.rollback()
        db.session.execute('SET statement_timeout = 0')

        if rows:
            user_ids = set()
            if "user_id" in columns:
                user_index = columns.index("user_id")
                user_ids = {row[user_index] for row in rows}
            elif "user_ids" in columns:
                users_index = columns.index("user_ids")
                for row in rows:
                    if row[users_index]:
                        for user_id_str in row[users_index].split(','):
                            user_ids.add(int(user_id_str.strip()))

            if user_ids:
                users = {user.id: user for user in User.query.filter(User.id.in_(user_ids))}

            m = re.search(r'\sorder\s+by\s+([a-zA-Z0-9_.]+)\s*$', query.sql, re.IGNORECASE)
            x_column = m[1].split('.')[-1] if m else None
            if x_column and x_column in columns:
                x_index = columns.index(x_column)
                if all(index == x_index or type(value) in (int,float,Decimal) for index, value in enumerate(rows[0])) and \
                    all(index == x_index or not column.endswith('_id') for index, column in enumerate(columns)):
                    # All columns (except perhaps the x_index) are floats or ints and aren't called '<something>_id'.
                    labels = [str(row[x_index]) for row in rows]
                    series = []
                    for index, column in enumerate(columns):
                        if index == x_index:
                            pass
                        else:
                            series.append({"name": column, "data": [float(row[index]) for row in rows]})
                    graph = {"labels": labels, "series": series, "minY": min(0,min(series[0]["data"]))}

    return flask.render_template('dashboard.html',
        queries=get_queries(),
        rows=rows,
        columns=columns,
        error=error,
        graph=graph,
        query_form=query_form,
        query_object=query,
        query_id=query.id,
        users=users
    )
