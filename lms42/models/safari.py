
import datetime
from ..app import db
import sqlalchemy as sa


safari_company_day_students = db.Table(
    'safari_offer_students',
    db.Column('safari_offer_id', db.Integer, db.ForeignKey('safari_offer.id')),
    db.Column('student_id', db.Integer, db.ForeignKey('user.id'))
)


class Safari(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    first_date = sa.Column(sa.Date, nullable=False)
    second_date = sa.Column(sa.Date, nullable=False)
    company_signup_end_date = sa.Column(sa.Date, nullable=False)
    student_signup_end_date = sa.Column(sa.Date, nullable=False)
    
    offers = sa.orm.relationship('SafariOffer', backref='safari', lazy='joined', cascade="all, delete-orphan")

    @property
    def company_signup_open(self):
        return self.company_signup_end_date >= datetime.date.today()

    @property
    def student_signup_open(self):
        return self.student_signup_end_date >= datetime.date.today()
    
    def get_offer(self, company_id):
        return SafariOffer.query.filter_by(safari_id=self.id, company_id=company_id).first()


class SafariOffer(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)
    company_id = sa.Column(sa.Integer, sa.ForeignKey('company.id'), nullable=False)
    company = sa.orm.relationship('Company')
    safari_id = sa.Column(sa.Integer, sa.ForeignKey('safari.id'), nullable=False)

    spots = sa.Column(sa.Integer, nullable=False)
    use_alt_day = sa.Column(sa.Boolean, nullable=False, default=False)

    contact_person = sa.Column(sa.String, nullable=False)
    contact_person_info = sa.Column(sa.String, nullable=False)
    extra_information = sa.Column(sa.String, nullable=False)
    
    students = sa.orm.relationship('User', secondary=safari_company_day_students, backref='safari_offers')
