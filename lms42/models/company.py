import flask
import datetime
import sqlalchemy as sa
from ..app import db, get_base_url
from ..utils import generate_password
from ..models.user import LoginLink


class Company(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    name = sa.Column(sa.String, nullable=False)
    logo = sa.Column(sa.String)
    logo_includes_name = sa.Column(sa.Boolean, nullable=False, default=False)
    background_color = sa.Column(sa.String, default='#ffffff', nullable=False)
    foreground_color = sa.Column(sa.String, default='#444444', nullable=False)

    city = sa.Column(sa.String)
    address = sa.Column(sa.String)

    description = sa.Column(sa.String)
    size = sa.Column(sa.Integer)
    developers = sa.Column(sa.Integer)
    promotext = sa.Column(sa.String)
    technologies = sa.Column(sa.String)

    intern_welcome = sa.Column(sa.Boolean, default=False, nullable=False)
    intern_fee = sa.Column(sa.Integer)
    english_welcome = sa.Column(sa.Boolean, default=False, nullable=False)
    intern_apply = sa.Column(sa.String)

    company_publish = sa.Column(sa.Boolean, default=False, nullable=False)
    teacher_publish = sa.Column(sa.Boolean, default=True, nullable=False)

    @property
    def logo_file(self):
        if self.logo:
            return f"/static/logos/{self.logo}"
        return None

    @property
    def safari_contacts(self):
        return CompanyContact.query.filter_by(safari_messages=True, company_id=self.id)
    
    def create_login_link(self):
        link = LoginLink()
        link.secret = generate_password()
        link.time = datetime.datetime.utcnow() + datetime.timedelta(days=28)
        link.company = self
        link.redirect_url = "/companies"
        db.session.add(link)
        db.session.commit()
        return get_base_url() + f"/user/link/{link.id}/{link.secret}"



class CompanyContact(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    company_id = sa.Column(sa.Integer, sa.ForeignKey('company.id'))
    company = sa.orm.relationship("Company", backref="contacts")

    first_name = sa.Column(sa.String, nullable=False, default='')
    last_name = sa.Column(sa.String, nullable=False, default='')
    role = sa.Column(sa.String, nullable=False, default='')
    email = sa.Column(sa.String, nullable=False)
    phone = sa.Column(sa.String)
    student_visible = sa.Column(sa.Boolean, default=True, nullable=False)
    safari_messages = sa.Column(sa.Boolean, default=True, nullable=False)
