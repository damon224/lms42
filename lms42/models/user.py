from ..app import app, db, scheduler, retry_commit
from .. import working_days
import flask_login
import datetime
import os
import secrets
import base64
import sqlalchemy as sa
import flask
from sqlalchemy.ext.hybrid import hybrid_property
from backports.cached_property import cached_property

STUDENT_LEVEL = 10

AVATARS_DIR = os.path.join(app.config['DATA_DIR'], "avatars")
os.makedirs(AVATARS_DIR, exist_ok=True)


class User(db.Model):  # noqa: PLR0904
    id = sa.Column(sa.Integer, primary_key=True)

    first_name = sa.Column(sa.String, nullable=False)
    last_name = sa.Column(sa.String, nullable=False)
    
    email = sa.Column(sa.String, nullable=False, unique=True)
    @sa.orm.validates('email')
    def lower_case_email(self, key, value):
        return value.lower()

    level = sa.Column(sa.SmallInteger, nullable=False, default=STUDENT_LEVEL)
    # 10 student
    # 30 inspector
    # 50 teacher
    # 80 admin (allows impersonation)
    # 90 owner

    student_number = sa.Column(sa.Integer, unique=True)

    short_name = sa.Column(sa.String, nullable=False, unique=True)
    @sa.orm.validates('short_name')
    def lower_case_short_name(self, key, value):
        return value.lower()

    counselor_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    counselor = sa.orm.relationship("User", foreign_keys=[counselor_id], backref="counselees", remote_side=[id])

    attempts = sa.orm.relationship("Attempt", lazy='dynamic', foreign_keys="Attempt.student_id", back_populates="student")

    current_attempt_id = sa.Column(sa.Integer, sa.ForeignKey('attempt.id', use_alter=True))
    current_attempt = sa.orm.relationship("Attempt", foreign_keys="User.current_attempt_id")

    default_schedule = sa.Column(sa.ARRAY(sa.String), nullable=False, default=['present', 'present', 'present', 'present', 'present'])

    avatar_name = sa.Column(sa.String)

    class_name = sa.Column(sa.String, nullable=False, default='')

    start_date = sa.Column(sa.String, nullable=False, default=datetime.date.today)

    is_hidden = sa.Column(sa.Boolean, nullable=False, default=False)
    english = sa.Column(sa.Boolean, nullable=False, default=False)

    discord_id = sa.Column(sa.String, nullable=True)

    discord_notification_same_exercise = sa.Column(sa.Boolean, nullable=False, default=True)
    discord_notification_exam_reviewed = sa.Column(sa.Boolean, nullable=False, default=True)

    needs_check_in = sa.Column(sa.DateTime)

    ai_credits = sa.Column(sa.Integer, nullable=False, default=0)

    __table_args__ = (
        sa.Index('user_type_index', 'is_active', 'level', 'is_hidden'),
    )

    # The following properties and method are required by Flask Login:
    is_active = sa.Column(sa.Boolean, nullable=False, default=True)
    is_anonymous = False

    @property
    def paused_attempt(self):
        return Attempt.query.filter_by(student_id=self.id).filter_by(status='paused').first()

    @property
    def leniency_targets(self):
        lt = LeniencyTargets.query.filter_by(student_id=self.id).order_by(LeniencyTargets.time.desc()).limit(1).first()
        return lt.targets if lt else ''

    @property
    def is_authenticated(self):
        return self.is_active

    def get_id(self):
        return str(self.id)

    def get_ects(self):
        passed_exams = Attempt.query \
                .filter_by(student_id=self.id, status='passed') \
                .filter(Attempt.credits > 0) \
                .distinct(Attempt.node_id) \
                .subquery()

        return db.session.query(sa.sql.func.sum(passed_exams.c.credits)).scalar() or 0

    @hybrid_property
    def is_fake(self):
        return "@" not in self.email

    @hybrid_property
    def is_student(self):
        return self.level == STUDENT_LEVEL

    @hybrid_property
    def is_inspector(self):
        return self.level >= 30

    @hybrid_property
    def is_teacher(self):
        return self.level >= 50

    @hybrid_property
    def is_admin(self):
        return self.level >= 80
        
    @hybrid_property
    def is_owner(self):
        return self.level >= 90

    @property
    def avatar(self):
        if self.avatar_name:
            return f"/static/avatars/{self.avatar_name}"
        else:
            return "/static/placeholder.png"

    @property
    def avatar_path(self):
        return os.path.join(AVATARS_DIR, self.avatar_name)

    @avatar.setter
    def avatar(self, data_uri):
        # TODO: this is not transaction safe.
        if self.avatar_name:
            try:
                os.unlink(self.avatar_path)
            except Exception:
                print(f"Unlink {self.avatar_path} failed")
            self.avatar_name = None

        if not data_uri:
            return

        comma = data_uri.find(';base64,')
        if comma:
            header = data_uri[0:comma]
            image = base64.b64decode(data_uri[comma+8:])
            ext = ".jpg" if header.find("/jpeg") else (".png" if header.find("/png") else None)
            if ext:
                self.avatar_name = secrets.token_urlsafe(8) + ext
                with open(self.avatar_path, "wb") as file:
                    file.write(image)
                return
        raise Exception(f"Couldn't parse avatar data url starting with {data_uri[0:40]}")


    @property
    def description(self):
        if self.is_teacher:
            return "Teacher"
        if self.is_inspector:
            return "Inspector"
        if self.current_attempt_id:
            node = curriculum.get('nodes_by_id').get(self.current_attempt.node_id)
            if not node:
                return self.current_attempt.node_id
            topic = curriculum.get('modules_by_id')[node["module_id"]]
            return ((topic.get('short') or topic.get('name')) + " ➤ " if topic.get('name') else '') + node['name']
        return 'Idle student'

    @property
    def full_name(self):
        return (self.first_name + " " + self.last_name).strip()

    @property
    def url_query(self):
        if flask_login.current_user.id != self.id:
            return f"?student={self.id}"
        return ''

    def is_present(self, before_time=None):
        """True when the student has logged in from within a Saxion building today."""
        time_log = TimeLog.query \
            .filter_by(date=datetime.date.today()) \
            .filter_by(user_id=self.id) \
            .first()
        if time_log is None:
            return False
        if before_time and time_log.start_time > before_time:
            return False
        return True
    
    def get_presence_class(self):
        time_log = TimeLog.query \
            .filter_by(date = datetime.date.today()) \
            .filter_by(user_id = self.id) \
            .first()
        if time_log is None:
            return "away"
        now = datetime.datetime.now() - datetime.timedelta(minutes=10)
        return "online" if time_log.end_time > now.time() else "today"

    @cached_property
    def performance(self):
        return Performance(self)

    def query_hours(self, start_date = "1970-01-01", end_date = datetime.datetime.today(), period = 'YYYY-MM'):
        sql = sa.text("""
        select
            to_char(date, :period) as period,
            extract(epoch from sum(
                LEAST(end_time, CAST('18:00:00' AS TIME WITHOUT TIME ZONE))
                -
                GREATEST(LEAST(start_time, CAST('19:00:00' AS TIME WITHOUT TIME ZONE)), CAST('08:00:00' AS TIME WITHOUT TIME ZONE))
            ))+30*count(*) seconds
        from time_log
        where user_id = :user_id and date >= :start_date and date < :end_date
        group by period
        """)

        with db.engine.connect() as dbc:
            return dbc.execute(sql, user_id=self.id, start_date=start_date, end_date=end_date, period=period).fetchall()

    def get_attempts(self):
        if self.is_teacher: # user is teacher, so return attempt reviews
            return Attempt.query.join(Grading).filter_by(grader_id=self.id).order_by(Grading.time.desc()).limit(10).all()
        else: # else user is student, so just get the attempts
            return Attempt.query.filter_by(student_id=self.id).order_by(Attempt.submit_time.desc()).limit(10).all()
    
    # Just to make REPL work more pleasant:
    def __repr__(self):
        return '<User {}: {}>'.format(self.id, self.short_name)



class LoginLink(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)
    time = sa.Column(sa.DateTime, default=lambda: datetime.datetime.utcnow() + datetime.timedelta(hours=1), nullable=False)
    secret = sa.Column(sa.String, nullable=False)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    user = sa.orm.relationship('User')

    company_id = sa.Column(sa.Integer, sa.ForeignKey('company.id'))
    company = sa.orm.relationship('Company')

    redirect_url = sa.Column(sa.String)

    def login(self):
        if self.user:
            if flask.session.get('company_id'):
                del flask.session['company_id']
            flask_login.login_user(self.user)
        else: # company
            flask_login.logout_user()
            flask.session['company_id'] = self.company.id
            flask.flash(f"Successfully logged in as representative of {self.company.name}")
        return flask.redirect(self.redirect_url)



class LeniencyTargets(db.Model):
    student_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False, primary_key=True)
    time = sa.Column(sa.DateTime, default=datetime.datetime.utcnow, nullable=False, primary_key=True)

    teacher_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False)
    targets = sa.Column(sa.String, nullable=False)



SOME_DATE = datetime.date(2020, 1, 1)
MIN_START_TIME = datetime.time(8, 0)
MAX_END_TIME = datetime.time(18, 0)

class TimeLog(db.Model):
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False, primary_key=True)
    user = sa.orm.relationship('User')

    date = sa.Column(sa.Date, nullable=False, primary_key=True, index=True)

    start_time = sa.Column(sa.Time, nullable=False)
    end_time = sa.Column(sa.Time, nullable=False)
    suspicious = sa.Column(sa.Boolean, nullable=False, default=False)

    @property
    def hours(self):
        start = datetime.datetime.combine(SOME_DATE, min(max(self.start_time, MIN_START_TIME), MAX_END_TIME))
        end = datetime.datetime.combine(SOME_DATE, min(max(self.end_time, MIN_START_TIME), MAX_END_TIME))
        result = (end - start).total_seconds() / 3600
        if self.suspicious:
            result /= 2
        return result


def get_students(everybody=False, class_filter="%", show_hidden=False, show_inactive=False, students_only=True, exams_only=False):
    
    non_exam_queries = "" if exams_only else """
select
    student_id,
    'approval' tag,
    'important' as color,
    10 as prio,
    attempt.start_time as prio_time
from attempt
where attempt.status='awaiting_approval'

union

select
    id,
    'check-in' tag,
    'info' as color,
    8 prio,
    needs_check_in as prio_time
from "user"
where needs_check_in is not null

union

select
    student_id,
    (case when attempt.status='needs_consent' then node_id||' 👀' else node_id end) as tag,
    (case when credits>0 then 'warning' else 'default' end) as color,
    (case when attempt.status='needs_consent' then 6 when attempt.credits>0 then 1 else 0 end) as prio,
    attempt.submit_time as prio_time
from attempt
where (attempt.status='needs_grading' or attempt.status='needs_consent')

union
"""

    # We're only returning users with a max tag prio of at least 1, unless `everybody` is True.
    query = sa.sql.text(f"""
select
    id,
    short_name,
    (case when avatar_name is null then '/static/placeholder.png' else '/static/avatars/' || avatar_name end) as avatar,
    class_name,
    array_remove(array_agg(tag || ':' || color order by prio desc, prio_time asc), null) as tags,
    (select end_time>(localtime at time zone 'Europe/Amsterdam')::time - interval '5 minutes' from time_log where time_log.user_id=student.id and date=current_date) as online
from "user" as student
left join (
    {non_exam_queries}

    select
        student_id,
        (case when extract(epoch from (now() - attempt.last_screenshot_time)) > 65 then 'no recording' when now() > attempt.deadline_time then 'exam deadline' else 'taking exam' end) tag,
        (case when extract(epoch from (now() - attempt.last_screenshot_time)) > 65 then '*warning' when now() > attempt.deadline_time then '*important' else 'ok' end) as color,
        (case when extract(epoch from (now() - attempt.last_screenshot_time)) > 65 then 20 when now() > attempt.deadline_time then 21 else 2 end) as prio,
        attempt.deadline_time as prio_time
    from attempt
    where attempt.status='in_progress' and attempt.credits>0
) as tags on tags.student_id = student.id

where level<=:max_level{"" if show_hidden else " and is_hidden=false"}{"" if show_inactive else " and is_active=true"} and class_name LIKE :class_filter
group by id
{"" if everybody else "having max(prio) > 0"}
order by class_name nulls last, {"short_name" if everybody else "max(prio) desc, min(prio_time) asc"}
;
""")

    cutoff = datetime.datetime.now().replace(hour=10, minute=30)
    return db.session.execute(query, {
        "class_filter": class_filter,
        "cutoff_time": cutoff,
        "max_level": 10 if students_only else 99,
    }).fetchall()



class AbsentDay(db.Model):
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False, primary_key=True)
    user = sa.orm.relationship('User', foreign_keys="AbsentDay.user_id")

    date = sa.Column(sa.Date, nullable=False, primary_key=True, index=True)
    
    reason = sa.Column(sa.String, nullable=False)
    reason_history = sa.Column(sa.String, nullable=False, default='')
    exempt_time = sa.Column(sa.Boolean, nullable=False, default=False)
    exempt_progress = sa.Column(sa.Boolean, nullable=False, default=False)

    by_user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=True)
    by_user = sa.orm.relationship('User', foreign_keys="AbsentDay.by_user_id")

    @staticmethod
    def log(user_id, date, reason, exempt_time=None, exempt_progress=None):
        absent_day = AbsentDay.query.filter_by(user_id=user_id, date=date).first()
        if not absent_day:
            absent_day = AbsentDay(
                user_id=user_id,
                date=date,
                reason_history='',
            )
            db.session.add(absent_day)
        absent_day.reason = reason
        if absent_day.reason_history:
            absent_day.reason_history += "\n\n"
        absent_day.reason_history += datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
        if flask_login.current_user.is_authenticated:
            absent_day.reason_history += " by=" + flask_login.current_user.short_name
        if exempt_time is not None:
            absent_day.exempt_time = exempt_time
            absent_day.reason_history += f" exempt_time={exempt_time}"
        if exempt_progress is not None:
            absent_day.exempt_progress = exempt_progress
            absent_day.reason_history += f" exempt_progress={exempt_progress}"
        absent_day.reason_history += f": {reason}"


from ..models import curriculum
from .attempt import Attempt

@app.before_first_request
def save_structural_days_as_absent_day():
    """Saves an absent_day in the database when the user has an absent day in his default schedule 
    and today is a working day."""
    today = datetime.date.today()
    day_number = today.weekday()
    if not working_days.is_working_day(today):
        # If today is not a working day, then there is no need to save an absent day.
        return
    for user in User.query.all():
        if user.default_schedule[day_number] != 'present' and not AbsentDay.query.filter_by(date=today, user_id=user.id).first():
            reason = user.default_schedule[day_number]
            AbsentDay.log(user.id, today, 'other obligations' if reason == 'absent_structural' else 'work from home', exempt_time=True, exempt_progress=(reason == 'absent_structural'))
            db.session.commit()

# Every weekday (mon-fri) in the middle of the night the non-present days that are in the default schedule of the user, 
# are stored as absent_day. (From: https://stackoverflow.com/a/38501429)
scheduler.add_job(func=save_structural_days_as_absent_day, trigger="cron", day_of_week='mon-fri', hour=2, minute=15, second=0)

@retry_commit
def user_nightly():
    print("Unset needs_check_in")
    query = sa.text("""update public.user
    SET needs_check_in = null
    WHERE level = :level""")
    with db.engine.connect() as dbc:
        dbc.execute(query, level=STUDENT_LEVEL)

scheduler.add_job(func=user_nightly, trigger="cron", day_of_week="mon-fri", hour=2, minute=0, second=0)



from .performance import Performance
from .grading import Grading
