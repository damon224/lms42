import datetime
import math
import sqlalchemy as sa
from ..app import db


class AIQuery(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)
    time = sa.Column(sa.DateTime, default=datetime.datetime.utcnow, nullable=False)

    attempt_id = sa.Column(sa.Integer, sa.ForeignKey('attempt.id'))
    attempt = sa.orm.relationship("Attempt")

    request_user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False)
    request_user = sa.orm.relationship("User")

    topic = sa.Column(sa.String)
    model = sa.Column(sa.String, nullable=False)
    messages = sa.Column(sa.JSON, nullable=False)

    answer = sa.Column(sa.String, nullable=False)
    finish_reason = sa.Column(sa.String, nullable=False)
    prompt_tokens = sa.Column(sa.Integer, nullable=False)
    completion_tokens = sa.Column(sa.Integer, nullable=False)
    
    @property
    def milli_dollars(self):
        if self.model.startswith("gpt-3.5-turbo-16k"):
            in_price, out_price = 3, 4
        elif self.model.startswith("gpt-3.5-turbo"):
            in_price, out_price = 1, 2
        elif self.model == 'gpt-4-1106-preview':
            in_price, out_price = 10, 30
        elif self.model == 'claude-2.1':
            in_price, out_price = 8, 24
        elif self.model.startswith("gpt-4-32k"):
            in_price, out_price = 60, 120
        elif self.model.startswith("gpt-4"):
            in_price, out_price = 30, 60
        else:
            raise ValueError(f"Unknown model price for {self.modal}")

        return math.ceil(in_price * self.prompt_tokens / 1000 + out_price * self.completion_tokens / 1000)
