"""Provides forms for the company system.

There are two forms - one for a Company and one for a Team.
They match with the Company and Team database models.
"""

import flask_wtf
import wtforms as wtf
from flask_wtf.file import FileAllowed, FileField
from wtforms import validators as wtv


class CompanyContactForm(flask_wtf.FlaskForm):
    first_name = wtf.StringField("First name")
    last_name = wtf.StringField("Last name")
    role = wtf.StringField("Role")
    email = wtf.StringField("Email (required)", validators=[wtv.InputRequired()])
    phone = wtf.StringField("Phone number")
    student_visible = wtf.BooleanField('Visible for students')
    safari_messages = wtf.BooleanField('Receive email about Business Safaris')

    submit = wtf.SubmitField('Save')


class AddCompanyForm(flask_wtf.FlaskForm):
    name = wtf.StringField('Company name', validators=[wtv.InputRequired()])
    submit = wtf.SubmitField('Save')


class SeparatorField(wtf.HiddenField):
    pass


class EditCompanyForm(flask_wtf.FlaskForm):
    """Provides a form for editing/adding a company to the company system."""

    sep1 = SeparatorField("Basic info")

    name = wtf.StringField('Company name', validators=[wtv.InputRequired()])
    logo = FileField(
        'Logo — Highly recommended to stand out!',
        validators=[FileAllowed(['png'], 'PNG files only.')],
    )
    logo_includes_name = wtf.BooleanField("The logo contains the company name (otherwise the name will be displayed besides the logo)")
    foreground_color = wtf.ColorField("Brand text color", default="#444444")
    background_color = wtf.ColorField("Brand background color", default="#ffffff")
    city = wtf.StringField('City')
    address = wtf.StringField('Street address')

    sep2 = SeparatorField("About the company")
    description = wtf.StringField('Short description of what the company does', validators=[
        wtv.InputRequired(),
    ])
    size = wtf.IntegerField('(Approximate) company head count')
    developers = wtf.IntegerField('Of which, how many software developers')
    promotext = wtf.TextAreaField("What makes this a great company to work for")
    technologies = wtf.TextAreaField("Languages/frameworks/technologies often used at the company")

    sep3 = SeparatorField("Internships")
    intern_welcome = wtf.BooleanField('The company can regularly accept Software Development interns (please ignore all further questions if not)')
    intern_fee = wtf.IntegerField(
        'Internship fee per month, based on 4 days a week, gross amount', validators=[wtv.Optional()]
    )
    english_welcome = wtf.BooleanField("International (English language) students are welcome", default=True)

    intern_apply = wtf.TextAreaField("How to apply for an internship")

    sep4 = SeparatorField("Publishing")
    company_publish = wtf.BooleanField('Make the company visible to students')

    submit = wtf.SubmitField('Save')


class EditCompanyFormTeacher(EditCompanyForm):
    teacher_publish = wtf.BooleanField('Teachers: *Actually* make the company visible to students. :-)')
