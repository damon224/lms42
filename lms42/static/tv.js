(function(){

    let nowE = document.getElementById('sched_now')
    let nextE = document.getElementById('sched_next')
    let timeE = document.getElementById('sched_time')


    if (Notification.permission != 'granted'){
        Notification.requestPermission();
    }

    let timesString = `
        08:30 - 09:00: Relaxed work
        09:00 - 09:30: Focus time / Train intro: ring.mp3
        09:30 - 10:20: Focus time
        10:20 - 10:40: Suggested coffee break @ lounge: coffee.mp3
        10:40 - 12:00: Focus time
        12:00 - 12:15: Ten-minute lecture & announcements: airhorn.mp3
        12:15 - 12:30: Mandatory lunch break
        12:30 - 13:00: Relaxed work / Extended lunch break
        13:00 - 14:20: Focus time: ring.mp3
        14:20 - 14:40: Suggested tea break @ lounge
        14:40 - 15:30: Focus time
        15:30 - 16:00: Focus time / Train outro / Teachers busy
        16:00 - 17:00: Relaxed work / Teachers busy
    `;
    
    let timetable = [];
    for(let line of timesString.trim().split("\n")) {
        let [times, description, sound] = line.trim().split(': ');
        times = times.split(' - ');
        timetable.push({start: times[0], end: times[1], description: description, sound: sound});
    }

    function calculateTimeDelta(from, to) {
        from = from.split(':').map(a => parseInt(a));
        to = to.split(':').map(a => parseInt(a));
        return (to[0] - from[0])*60 + to[1]-from[1] + " minutes";
    }

    function play(sample) {
        new Audio(`/static/${sample}`).play()
    }

    function notify(now){
        new Notification(now.description, {
            body: now.description + " until " + now.end,
            icon: '/static/favicon.svg'
        });
    }

    function updateSchedule() {
        var dateTime = new Date();
        time = ("0"+dateTime.getHours()).slice(-2) + ":" + ("0"+dateTime.getMinutes()).slice(-2);
        timeE.innerText = time;

        let nowIndex = 0;
        while(nowIndex+1<timetable.length && time >= timetable[nowIndex+1].start) {
            nowIndex++;
        }

        now = nowIndex < 0 ? null : timetable[nowIndex];
        next = nowIndex + 1 >= timetable.length ? null : timetable[nowIndex+1];

        nowE.innerText = now ? now.description : '';
        nextE.innerText = next ? next.description+" in "+calculateTimeDelta(time, next.start) : "Next: have a great evening!";

        if (now && time==now.start) {
            if (Notification.permission == 'granted'){
                notify(now);
            }
            if (now.sound) play(now.sound);
        }

        setTimeout(updateSchedule, (60.2 - dateTime.getSeconds())*1000);
    }

    updateSchedule();

    // // Reload every 2h
    // setTimeout(function() {
    //     location.reload();
    // }, 2*60*60*1000)
})();
