(function() {
    if (self.initUpdateScores) return;

    function format_grade(grade) {
        // One digit after the decimal point, rounded down.
        return (grade  - 0.0499999).toFixed(1)
    }

    self.initUpdateScores = function(scriptE, all_weights, floor, passing_grade) {
        let formE;
        for(formE = scriptE; true; formE = formE.parentNode) {
            if (formE.classList.contains("assignment")) {
                break;
            }
        }

        for(let inputE of formE.getElementsByTagName('input')) {
            inputE.addEventListener('input', updateScores)
        }

        updateScores();

        function updateScores() {
            let scoreRowE = formE.getElementsByClassName(`grading-scores`)[0];
            let gradeRowE = formE.getElementsByClassName(`grading-grade`)[0];
            let selectE = formE.querySelector('[name="formative_action"]') || {};
            let total = floor;
            let entry_fail = false;
            let grade_count = 0;
            let table_pos = 1;
            let incomplete = false;

            for(let rubric_num=0; rubric_num<all_weights.length; rubric_num++) {
                weight = all_weights[rubric_num];
                let grade;

                let numberE = formE.querySelector(`input[type="number"][name="score_${rubric_num}_scale10"]`);
                let radioE = formE.querySelector(`input[type="radio"][name="score_${rubric_num}"]:checked`);
                if (weight==null) { // MUST rubric
                    if (radioE && radioE.value!="yes") entry_fail = true;
                    continue;
                }
                if (numberE) {
                    grade = parseFloat(numberE.value);
                    if (isNaN(grade)) {
                        grade = undefined;
                    } else {
                        grade = (grade - 1) / 9;
                    }
                }
                else if (radioE) {
                    grade = radioE.value/4.0
                }
                grade_count++;

                let scoreText = '', gradeText = '';
                if (grade==null) {
                    incomplete = true;
                }
                else {
                    scoreText = Math.round(grade*100)+'%';
                    gradeText = format_grade(grade * weight);
                    total += grade * weight;
                }

                scoreRowE.children[table_pos].innerText = scoreText;
                gradeRowE.children[table_pos].innerText = gradeText;
                table_pos++;
            }

            if (entry_fail || Math.round(total+0.0001) < passing_grade) {
                if (!gradeRowE.classList.contains('failed')) {
                    gradeRowE.classList.add('failed');
                    // Let the teacher decide what to do...
                    selectE.value = '';
                }
            } else {
                gradeRowE.classList.remove('failed');
                if (!incomplete & !selectE.value) {
                    selectE.value = "passed";
                }
            }
            if (incomplete) {
                gradeRowE.classList.add('incomplete');
            } else {
                gradeRowE.classList.remove('incomplete');
            }
            gradeRowE.children[grade_count+2].innerText = format_grade(total)
        }
    }
})();
