'use strict'

// Tab bar 
function initTabs() {
    function getHashKey(titleElement) {
        return titleElement.innerText.toLowerCase().replace(/ /g,'_')
    }

    let selectorsList = document.getElementsByClassName('tab-selectors')
    let blocksList = document.getElementsByClassName('tab-blocks')

    for(let tabNum=0; tabNum<selectorsList.length; tabNum++) {
        if (selectorsList[tabNum].__tabInitialized) continue
        selectorsList[tabNum].__tabInitialized = true
        
        let selectors = selectorsList[tabNum].children
        let blocks = blocksList[tabNum].children

        let posByKey = {}
        
        let hash = decodeURI(location.hash.substr(1))
        let initialSelected

        for (let pos=0; pos<selectors.length; pos++) {
            selectors[pos].addEventListener('click', function() { selectBlock(pos); })
            blocks[pos].style.display = 'none'
            selectors[pos].classList.add('is-outlined')
            if (initialSelected==null && blocks[pos].classList.contains('tab-initial')) initialSelected = pos
            let key = getHashKey(selectors[pos])
            posByKey[key] = pos
            if (hash==key) {
                initialSelected = pos
            }
        }


        let selectedBlock = -1
        function selectBlock(num) {
            if (selectors[selectedBlock]) {
                selectors[selectedBlock].classList.remove('is-active')
                blocks[selectedBlock].style.display = 'none'
            }
            selectedBlock = num
            if (selectors[selectedBlock]) {
                selectors[selectedBlock].classList.add('is-active')
                blocks[selectedBlock].style.display = ''
                // Recalculate textarea heights when selecting tab
                for(let el of document.getElementsByTagName('TEXTAREA')) {
                    resizeTextArea(el)
                }
                window.history.replaceState('', '', location.pathname+location.search+'#'+encodeURI(getHashKey(selectors[selectedBlock])))
            }
        }
        selectBlock(initialSelected || 0)
        window.addEventListener('hashchange', function() {
            let pos = posByKey[decodeURI(location.hash.substr(1))]
            console.log('change', pos, location.hash)
            if (pos!=null) {
                selectBlock(pos)
            }
        })
    }
}
window.addEventListener('load', initTabs)

// Ping for attendance every minute
fetch('/time_log/ping')
setInterval(function() {
    fetch('/time_log/ping')
}, 60*1000)

// Give links to the current page the .current-page class
;(function(anchors, url) {
    for (let anchor of anchors) {
        if (anchor.href == url) {
            anchor.classList.add('current-page')
        }
    }
})(document.getElementsByTagName('a'), location.href.split('#')[0])

// Toggle dark mode
window.toggleDarkMode = function() {
    const cookieTheme = (document.cookie.split('; ').find(row => row.startsWith('theme=')) || '').split('=')[1]
    const agentTheme = window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light'
    const currentTheme = cookieTheme || agentTheme
    const newTheme = currentTheme == 'light' ? 'dark' : 'light'

    document.documentElement.setAttribute('data-theme', newTheme)

    if (newTheme == agentTheme) { // Delete cookie (we're switching to the browser default)
        document.cookie = "theme=; max-age=0; path=/"
    } else { // Set theme cookie
        document.cookie = `theme=${newTheme}; max-age=999999999; path=/`
    }
}

// Toggle tab/scroll layout
window.toggleCurriculumScroll = function() {
    const oldLayout = (document.cookie.split('; ').find(row => row.startsWith('layout=')) || '').split('=')[1]
    const newLayout = oldLayout == 'scroll' ? '' : 'scroll'
    document.cookie = `layout=${newLayout}; max-age=999999999; path=/`
    location.reload()
}

addEventListener('keydown', function(e) {
    if (e.key=='t' && e.altKey) {
        toggleDarkMode()
        e.preventDefault()
    }
})


// Show/hide menu
window.toggleMenu = function() {
    document.body.classList.toggle('show-dropdown-menu')
}
addEventListener('click', function() {
    if (document.body.classList.contains('show-dropdown-menu')) {
        toggleMenu()
    }
},true)


// Auto grow textareas
function resizeTextArea(el) {
    el.style.height = ""; /* Reset the height*/
    let cs = getComputedStyle(el)
    let borderHeight = parseInt(cs.getPropertyValue("border-top-width")) + parseInt(cs.getPropertyValue("border-bottom-width"))
    el.style.height = (borderHeight + el.scrollHeight) + "px"
}

document.addEventListener('input', function(event) {
    let el = event.target
    if (el.tagName === 'TEXTAREA') {
        resizeTextArea(el)
    }
})


function collapseNode(e) {
    let element = e.target
    if (element.tagName != "DIV") {
        element = element.parentElement
    }
    let parent = element.parentElement

    if (parent.classList.contains("collapsed")){
        parent.classList.remove("collapsed")
        parent.classList.add("uncollapsing")
        setTimeout(() => {
            parent.classList.remove("uncollapsing")
            parent.classList.add("uncollapsed")
        }, 500)
    }
    else if (parent.classList.contains("uncollapsed")) {
        parent.classList.remove("uncollapsed")
        parent.classList.add("collapsing")
        setTimeout(() => {
            parent.classList.remove("collapsing")
            parent.classList.add("collapsed")
        }, 500)
    }
}

function pageIsBackNavigated() 
{
    if (window.performance) {
        let navigationEntries = performance.getEntriesByType("navigation");
        if (navigationEntries.length > 0) {
            let type = navigationEntries[0].type;
            if (type === "back_forward") {
                return true;
            }
        }
    }
    return false;
}

window.addEventListener('load', function(){
    for(let el of document.getElementsByTagName('TEXTAREA')) {
        resizeTextArea(el)
    }

    let collapsables = document.querySelectorAll('.curriculum_scroll .node-title')
    for (let collapsable of collapsables) {
        // Remove first, in case of a second `load` event (which we're doing because of htmx swaps)
        collapsable.removeEventListener("click", collapseNode)
        collapsable.addEventListener("click", collapseNode)
    }

    let horizontalColumns = document.querySelector(".curriculum_scroll .columns")
    if (horizontalColumns) {
        if (sessionStorage.getItem('horCurPos') && pageIsBackNavigated()) {
            horizontalColumns.style.scrollBehavior = 'initial';
            horizontalColumns.scrollLeft = sessionStorage.getItem('horCurPos');
            horizontalColumns.style.scrollBehavior = '';
        }

        horizontalColumns.addEventListener('scroll', function() {
            sessionStorage.setItem('horCurPos', horizontalColumns.scrollLeft);
        });

        // Autofocus the columns so people can use arrow keys to scroll
        horizontalColumns.tabIndex = 1
        horizontalColumns.focus({preventScroll: true})

        // After a delay, cause all collapsed topics to add a snap target,
        // so that we'll start with a snap to the first uncollapsed module.
        setTimeout(() => {
            horizontalColumns.classList.add('after_init_snap')
        }, 100)
    } 
})


function makeNode(tagClasses='', parentElement, properties) {
	tagClasses = tagClasses.split(".")
	let node = document.createElement(tagClasses.shift() || 'div')
	if (tagClasses) node.classList.add(...tagClasses)
	if (parentElement) parentElement.appendChild(node)

	for (let name in properties||{}) {
        if (typeof properties[name] === 'function') {
            node.addEventListener(name, properties[name])
        } else if (name==='text') {
            node.innerText = properties[name]
        } else if (name==='html') {
            node.innerHTML = properties[name]
        } else {
		    node[name] = properties[name]
        }
	}
    
	return node
}


;(function() {

    let loadedScripts = {}
    window.loadScript = function(path, onLoad) {
        if (loadedScripts[path] == null) {
            loadedScripts[path] = onLoad ? [onLoad] : []
            var script = document.createElement('script')
            script.onload = function() {
                let onLoads = loadedScripts[path]
                loadedScripts[path] = true
                for(let onLoad of onLoads) onLoad()
            }
            script.src = path
            document.head.appendChild(script)
        }
        else {
            if (onLoad) {
                if (loadedScripts[path]===true) onLoad()
                else loadedScripts[path].push(onLoad)
            }
        }
    }

    let loadedCsses = {}
    window.loadCss = function(path) {
        if (loadedCsses[path]) return
        loadedCsses[path] = true
        var link = document.createElement('link')
        link.rel = 'stylesheet'
        link.href = path
        document.head.appendChild(link)
    }

    window.renderAsciinema = function(file, className,  opts) {
        opts = Object.assign({loop: true, poster: 'npt:0:10'}, opts || {})
        let currentScript = document.currentScript
        loadCss('/static/asciinema-player.css')
        loadScript('/static/asciinema-player.min.js', function() {
            console.log('init ascii')
            let el = document.createElement('div')
            if (className) {
                console.log('className: ' + className)
                el.classList.add(className)
            }
            currentScript.after(el)
            AsciinemaPlayer.create(file, el, opts)
        })
    }
})()